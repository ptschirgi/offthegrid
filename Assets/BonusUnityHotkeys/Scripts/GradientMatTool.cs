using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GradientMatTool : MonoBehaviour
{
    public SkinnedMeshRenderer sMsh;
    public MeshRenderer mMsh;

    public float min;

    public float sSpread;
    public float max;

    [Range(0f, 1f)]
    public float exVal; //0 - 1
}
