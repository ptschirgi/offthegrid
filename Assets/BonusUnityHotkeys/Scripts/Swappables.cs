using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SwapSet
{
    public string tagNm;
    public List<GameObject> swapFabs = new List<GameObject>();
}

[System.Serializable]
public class UiTray
{
    public GameObject thing;
    public float locoOffsetMult;
    // public Vector3 locoOffset;
}

[ExecuteInEditMode]
public class Swappables : MonoBehaviour
{
    public List<SwapSet> ssets = new List<SwapSet>();

    public List<GameObject> tempGs = new List<GameObject>();
    //tempG
    public bool isSwapping;
    public bool initOnce;

    public int scrolly;

    public Transform swapIcon;
    public Transform selectVis;
    public List<UiTray> visSwaps = new List<UiTray>();
}
