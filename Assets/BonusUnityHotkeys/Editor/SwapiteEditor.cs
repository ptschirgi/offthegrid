using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Swapite)), CanEditMultipleObjects]
public class SwapiteEditor : Editor
{
    public Swapite sp;


    void OnSceneGUI()
    {

        sp = (Swapite)target;

        if (sp.swps == null)
        {
            sp.swps = GameObject.FindGameObjectWithTag("Swappables").GetComponent<Swappables>();
        }
        if (!sp)
        {
            return;
        }



        if (sp.swps.isSwapping)
        {

            Transform camScene = SceneView.GetAllSceneCameras()[0].transform;
            sp.swps.swapIcon.position = camScene.position + (camScene.forward * 1.5f) + (camScene.up * .66f) + (camScene.right * .66f);

            int sTile = 0;

            for (int i = 0; i < sp.swps.ssets.Count; i++)
            {
                if (sp.swps.ssets[i].tagNm == sp.nm)
                {
                    sTile = i;
                }
            }

            if (!sp.swps.initOnce)
            {
                sp.swps.scrolly = sp.myScrolly;
                sp.swps.initOnce = true;
                sp.swps.swapIcon.gameObject.SetActive(true);
                sp.swps.selectVis.gameObject.SetActive(true);
                sp.swps.swapIcon.localScale = new Vector3(.25f, .25f, .25f);

                float camSpotsDwn = 0f;//sp.swps.swapIcon.position

                for (int i = 0; i < sp.swps.ssets[sTile].swapFabs.Count; i++)
                {
                    camSpotsDwn -= .29f;
                    // Vector3 bnds = selection.GetComponent<MeshRenderer>().bounds.size;
                    Vector3 offSetDyn = (camScene.up * -.2f) + (camSpotsDwn * camScene.up);
                    GameObject newEmpty = Instantiate(sp.swps.ssets[sTile].swapFabs[i], offSetDyn, sp.transform.rotation);

                    newEmpty.transform.localScale = new Vector3(.035f, .035f, .035f);

                    UiTray newUiT = new UiTray();

                    newUiT.thing = newEmpty;
                    newUiT.locoOffsetMult = camSpotsDwn;

                    sp.swps.visSwaps.Add(newUiT);
                }
            }

            for (int i = 0; i < sp.swps.visSwaps.Count; i++)
            {

                Vector3 offSetDyn = (camScene.up * -.2f) + (sp.swps.visSwaps[i].locoOffsetMult * camScene.up);
                sp.swps.visSwaps[i].thing.transform.position = sp.swps.swapIcon.position + offSetDyn;
                sp.swps.visSwaps[i].thing.transform.rotation = sp.transform.rotation;
            }

            sp.swps.selectVis.position = sp.swps.visSwaps[sp.swps.scrolly].thing.transform.position + (camScene.up * .09f);// + (camScene.forward * .05f)
            sp.swps.selectVis.rotation = sp.transform.rotation;
            sp.swps.selectVis.localScale = new Vector3(.225f, .225f, .225f);//.015f

            EditorWindow view = EditorWindow.GetWindow<SceneView>();
            view.Repaint();

            List<GameObject> listGos = new List<GameObject>();

            foreach (var selection in Selection.gameObjects)
            {
                listGos.Add(selection);
            }

            //             List<GameObject> finalOutGos = new List<GameObject>();

            //             for (int i = 0; i < sp.swps.tempGs.Count; i++)
            //             {
            //                 if (listGos.Contains(sp.swps.tempGs[i]))
            //                 {
            // finalOutGos.Add()
            //                 }

            //             }

            //             if (!sp.swps.tempGs.Contains(sp.gameObject))
            //             {
            //                 sp.swps.tempGs.Add(sp.gameObject);
            //             }

            sp.swps.tempGs = listGos;

            Event e = Event.current;
            switch (e.type)
            {
                case EventType.ScrollWheel:
                    if (e.delta.y > 0f)
                    {
                        if (sp.swps.scrolly < sp.swps.ssets[sTile].swapFabs.Count - 1)
                        {
                            sp.swps.scrolly++;
                            for (int i = 0; i < sp.swps.tempGs.Count; i++)
                            {
                                sp.swps.tempGs[i].GetComponent<MeshFilter>().sharedMesh = sp.swps.ssets[sTile].swapFabs[sp.swps.scrolly].GetComponent<MeshFilter>().sharedMesh;
                            }
                        }
                    }
                    else if (e.delta.y < 0f)
                    {
                        if (sp.swps.scrolly > 0)
                        {
                            sp.swps.scrolly--;
                            for (int i = 0; i < sp.swps.tempGs.Count; i++)
                            {
                                sp.swps.tempGs[i].GetComponent<MeshFilter>().sharedMesh = sp.swps.ssets[sTile].swapFabs[sp.swps.scrolly].GetComponent<MeshFilter>().sharedMesh;
                            }
                        }
                    }

                    break;
            }

            if (Event.current.type == EventType.ScrollWheel) Event.current.Use();
        }
        else
        {
            if (sp.swps.initOnce)
            {
                sp.swps.swapIcon.gameObject.SetActive(false);
                sp.swps.selectVis.gameObject.SetActive(false);

                sp.swps.initOnce = false;

                for (int i = 0; i < sp.swps.visSwaps.Count; i++)
                {
                    DestroyImmediate(sp.swps.visSwaps[i].thing);
                }
                sp.swps.visSwaps.Clear();
            }
        }
    }
}
