using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;



[CustomEditor(typeof(GradientMatTool))]
public class GradientMatToolEditor : Editor
{
    public GradientMatTool gMat;

    public override void OnInspectorGUI()
    {
        gMat = (GradientMatTool)target;

        float range = gMat.max - gMat.min;


        float bot = gMat.min + (range * gMat.exVal);
        float top = (gMat.sSpread + gMat.min) + (range * gMat.exVal);

        if (gMat.sMsh != null)
        {

            gMat.sMsh.sharedMaterial.SetFloat("_3Pos", bot);
            gMat.sMsh.sharedMaterial.SetFloat("_4Pos", top);
        }
        if (gMat.mMsh != null)
        {
            gMat.mMsh.sharedMaterial.SetFloat("_3Pos", bot);
            gMat.mMsh.sharedMaterial.SetFloat("_4Pos", top);
        }

        DrawDefaultInspector();
    }
}
