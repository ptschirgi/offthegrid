﻿//Gameready assets, tips & tricks, tutorials www.not-lonely.com
//paul helped also!
//Usage:
//Put this script to Assets/Editor/

//Hotkeys:
//Deselect all by pressing Shift + D
//Create a Cube ahead the scene camera by pressing Shift + 1
//Create a Point Light ahead the scene camera by pressing Shift + 2
//Create a Spotlight ahead the scene camera by pressing Shift + 3
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEditor;
using UnityEditor.ProjectWindowCallback;


public class ExtendedHotkeys : ScriptableObject
{

    struct TransformData
    {
        public Vector3 localPosition;
        public Quaternion localRotation;
        public Vector3 localScale;

        public TransformData(Vector3 localPosition, Quaternion localRotation, Vector3 localScale)
        {
            this.localPosition = localPosition;
            this.localRotation = localRotation;
            this.localScale = localScale;
        }
    }

    private static TransformData _data;
    private static Vector3? _dataCenter;

    public static GameObject go;
    public static Vector3 goPos;

    public static bool toggleRendo;

    public static Favorites faves;
    public static Swappables swaps;

    public bool isCycling;
    public int targSet;

    [MenuItem("Edit/Copy Transform Values &c", false, -101)]
    public static void CopyTransformValues()
    {
        if (Selection.gameObjects.Length == 0) return;
        var selectionTr = Selection.gameObjects[0].transform;
        _data = new TransformData(selectionTr.localPosition, selectionTr.localRotation, selectionTr.localScale);
    }

    [MenuItem("Edit/Paste Transform Values &v", false, -101)]
    public static void PasteTransformValues()
    {
        foreach (var selection in Selection.gameObjects)
        {
            Transform selectionTr = selection.transform;
            Undo.RecordObject(selectionTr, "Paste Transform Values");
            selectionTr.localPosition = _data.localPosition;
            selectionTr.localRotation = _data.localRotation;
            selectionTr.localScale = _data.localScale;
        }
    }

    [MenuItem("Edit/Paste Transform Zero &x", false, -101)]
    public static void PasteTransformZero()
    {
        foreach (var selection in Selection.gameObjects)
        {
            Transform selectionTr = selection.transform;
            Undo.RecordObject(selectionTr, "Paste Transform Zero");
            selectionTr.localPosition = Vector3.zero;
            selectionTr.localRotation = Quaternion.identity;
            selectionTr.localScale = Vector3.one;
        }
    }

    [MenuItem("Edit/Parent Up Lvl &z", false, -101)]
    public static void ParentUpLvl()
    {
        foreach (var selection in Selection.gameObjects)
        {
            Transform selectionTr = selection.transform;
            Undo.RecordObject(selectionTr, "Parent Up Lvl");
            if (selectionTr.parent != null)
            {
                if (selectionTr.parent.parent != null)
                {
                    selectionTr.parent = selectionTr.parent.parent;
                }
                else
                {
                    selectionTr.parent = null;
                }
            }

        }
    }


    //Deselect all by pressing Shift + D
    [MenuItem("DDemons/Deselect All #_d")]
    static void DoDeselect()
    {
        Selection.objects = new UnityEngine.Object[0];
    }

    //Deselect all by pressing Shift + D
    [MenuItem("DDemons/FixNameNumbers #_n")]
    static void FixNameNum()
    {
        for (int i = 0; i < Selection.objects.Length; i++)
        {
            if (Selection.objects[i].name.Contains("("))
            {
                char[] nm = Selection.objects[i].name.ToCharArray();

                string firstbit = "";
                string extractNum = "";

                bool afterParenthesis = false;
                bool afterParenthesis2 = false;
                for (int c = 1; c < nm.Length; c++)
                {
                    if (nm[c] != ("(").ToCharArray()[0])
                    {
                        if (!afterParenthesis2)
                        {
                            if (!afterParenthesis)
                            {
                                firstbit += nm[c];
                            }
                            else
                            {
                                if (nm[c] != (")").ToCharArray()[0])
                                {
                                    extractNum += nm[c];
                                }
                                else
                                {
                                    afterParenthesis2 = true;
                                }
                            }

                        }
                        else
                        {



                        }
                    }
                    else
                    {
                        afterParenthesis = true;
                    }


                }

                Selection.objects[i].name = extractNum + firstbit;
            }
        }
        Selection.objects = new UnityEngine.Object[0];
    }

    //Create a Cube ahead the scene camera by pressing Shift + 1
    [MenuItem("DDemons/Create Cube #_1")]
    static void AddCube()
    {
        go = GameObject.CreatePrimitive(PrimitiveType.Cube);
        AfterCreation();
    }

    //Create a Point Light ahead the scene camera by pressing Shift + 2
    [MenuItem("DDemons/Create Point Light #_2")]
    static void AddPointLight()
    {
        go = new GameObject("Point Light");
        go.AddComponent<Light>().type = LightType.Point;
        AfterCreation();
    }

    //Create a Spotlight ahead the scene camera by pressing Shift + 3
    [MenuItem("DDemons/Create Spotlight #_3")]
    static void AddSpotlight()
    {
        go = new GameObject("Spotlight");
        go.AddComponent<Light>().type = LightType.Spot;
        go.transform.eulerAngles = new Vector3(90, 0, 0);
        AfterCreation();
    }
    static void AfterCreation()
    {
        goPos = SceneView.currentDrawingSceneView.camera.transform.TransformPoint(Vector3.forward * 1.1f);
        go.transform.position = goPos;
        Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
        Selection.activeObject = go;
    }

    [MenuItem("DDemons/Set Scroll Up Shift T")]
    static void SetScrollUp()
    {
        GameObject[] objs = UnityEditor.SceneManagement.EditorSceneManager.GetSceneAt(0).GetRootGameObjects();


        Selection.activeObject = objs[0];

        EditorGUIUtility.PingObject(objs[0]);
    }

    [MenuItem("DDemons/Set Scroll Down Shift B")]
    static void SetScrollDown()
    {
        GameObject[] objs = UnityEditor.SceneManagement.EditorSceneManager.GetSceneAt(0).GetRootGameObjects();


        Selection.activeObject = objs[objs.Length - 1];

        EditorGUIUtility.PingObject(objs[objs.Length - 1]);
    }

    [MenuItem("DDemons/Set Heir Bot B")]
    static void SetHeirarchyDown()
    {
        // Transform selectionTr = Selection.gameObjects[0].transform;


        foreach (var selection in Selection.gameObjects)
        {
            Transform selectionTr = selection.transform;
            selectionTr.SetAsLastSibling();


            Undo.RegisterFullObjectHierarchyUndo(selectionTr, "Parent Up Lvl");
        }

        EditorGUIUtility.PingObject(Selection.gameObjects[0]);

    }
    [MenuItem("DDemons/Set Heir Top &T")]
    static void SetHeirarchyTop()
    {
        // Transform selectionTr = Selection.gameObjects[0].transform;


        foreach (var selection in Selection.gameObjects)
        {
            Transform selectionTr = selection.transform;
            selectionTr.SetAsFirstSibling();

            Undo.RegisterFullObjectHierarchyUndo(selectionTr, "Parent Up Lvl");
        }

        EditorGUIUtility.PingObject(Selection.gameObjects[0]);

    }


    [MenuItem("DDemons/Move Obj to View V")]
    static void MoveToView()
    {

        Transform selectionTr0 = Selection.gameObjects[0].transform;
        // Undo.RecordObject(selectionTr, "Heir Down");

        Transform camScene = SceneView.GetAllSceneCameras()[0].transform;
        selectionTr0.position = camScene.position + (camScene.forward * 1.5f);
        Undo.RecordObject(selectionTr0, "Move View");


    }

    [MenuItem("DDemons/Set Root Parent P")]
    static void MakeRootParent()
    {

        Transform selectionTr0 = Selection.gameObjects[0].transform;
        // Undo.RecordObject(selectionTr, "Heir Down");

        GameObject newEmpty = new GameObject();
        newEmpty.name = "Root" + Selection.gameObjects[0].name;
        newEmpty.transform.position = selectionTr0.position;
        newEmpty.transform.rotation = selectionTr0.rotation;
        newEmpty.transform.parent = selectionTr0.parent;
        //    newEmpty.transform.localScale = selectionTr0.rotation;

        selectionTr0.parent = newEmpty.transform;


        EditorGUIUtility.PingObject(newEmpty);
    }

    [MenuItem("DDemons/DuplicateAlongMouse D")]
    static void DuplicateAlongMouse()
    {

        Vector3 mousePosition = Event.current.mousePosition;
        mousePosition.y = SceneView.lastActiveSceneView.camera.pixelHeight - mousePosition.y;


        Ray ray = SceneView.lastActiveSceneView.camera.ScreenPointToRay(mousePosition);

        GameObject selectionTr0 = Selection.gameObjects[0];
        float disto = ((selectionTr0.transform.position.y - SceneView.lastActiveSceneView.camera.transform.position.y) / ray.direction.y);

        Vector3 newPt = ray.GetPoint(disto);
        newPt.y = selectionTr0.transform.position.y;

        GameObject newEmpty = Instantiate(selectionTr0, newPt, selectionTr0.transform.rotation);

        string curNm = Selection.gameObjects[0].name;
        string newNm = "";
        char[] charNm = curNm.ToCharArray();

        if (System.Char.IsDigit(charNm[charNm.Length - 1]))
        {
            int num = int.Parse(charNm[charNm.Length - 1].ToString());

            num++;
            charNm[charNm.Length - 1] = num.ToString().ToCharArray()[0];
        }

        for (int i = 0; i < charNm.Length; i++)
        {
            newNm += charNm[i];
        }

        newEmpty.name = newNm;

        EditorGUIUtility.PingObject(newEmpty);

        Undo.RegisterCreatedObjectUndo(newEmpty, "newobj");
    }

    [MenuItem("DDemons/DuplicateSnapUpCam")]
    static void DuplicateSnapUpCam()
    {
        DupSnapUp(true);
    }

    [MenuItem("DDemons/DuplicateSnapUp")]
    static void DuplicateSnapUp()
    {
        DupSnapUp(false);
    }
    [MenuItem("DDemons/DuplicateSnapDownCam")]
    static void DuplicateSnapDownCam()
    {
        DupSnapDown(true);
    }

    [MenuItem("DDemons/DuplicateSnapDown")]
    static void DuplicateSnapDown()
    {
        DupSnapDown(false);
    }
    [MenuItem("DDemons/DuplicateSnapRightCam")]
    static void DuplicateSnapRightCam()
    {
        DupSnapRight(true);
    }

    [MenuItem("DDemons/DuplicateSnapRight")]
    static void DuplicateSnapRight()
    {
        DupSnapRight(false);
    }
    [MenuItem("DDemons/DuplicateSnapLeftCam")]
    static void DuplicateSnapLeftCam()
    {
        DupSnapLeft(true);
    }

    [MenuItem("DDemons/DuplicateSnapLeft")]
    static void DuplicateSnapLeft()
    {
        DupSnapLeft(false);
    }

    [MenuItem("DDemons/InvertScale & >")]
    static void InverseScl()
    {
        //
        foreach (var selection in Selection.gameObjects)
        {
            Transform selectionTr = selection.transform;

            Undo.RegisterCompleteObjectUndo(selectionTr, "inv rest");
            selectionTr.localScale = new Vector3(-selectionTr.localScale.x, selectionTr.localScale.y, selectionTr.localScale.z);

        }
        // Undo.FlushUndoRecordObjects();
    }

    [MenuItem("DDemons/InvertScaleZ & up")]
    static void InverseSclZ()
    {
        //
        foreach (var selection in Selection.gameObjects)
        {
            Transform selectionTr = selection.transform;

            Undo.RegisterCompleteObjectUndo(selectionTr, "inv restZ");
            selectionTr.localScale = new Vector3(selectionTr.localScale.x, selectionTr.localScale.y, -selectionTr.localScale.z);

        }
        // Undo.FlushUndoRecordObjects();
    }

    [MenuItem("DDemons/Rot90 & R")]
    static void Rotate90()
    {
        //
        foreach (var selection in Selection.gameObjects)
        {
            Transform selectionTr = selection.transform;

            Undo.RegisterCompleteObjectUndo(selectionTr, "inv restZ");
            selectionTr.Rotate(new Vector3(0, 90f, 0), Space.Self);

        }
        // Undo.FlushUndoRecordObjects();
    }

    static void DupSnapUp(bool bringCam)
    {
        Vector3 relativCamPos = Selection.gameObjects[0].transform.position - SceneView.lastActiveSceneView.pivot;
        Vector3 fwd = SceneView.lastActiveSceneView.camera.transform.forward;
        Vector3 globalaxis = Vector3.zero;


        bool doZ = false;

        if (Mathf.Abs(fwd.z) > Mathf.Abs(fwd.x))//only compare x and z?
        {
            if (fwd.z < 0)
            {
                globalaxis.z = -1f;
            }
            else
            {
                globalaxis.z = 1f;
            }
        }
        else
        {
            doZ = true;

            if (fwd.x < 0)
            {
                globalaxis.x = -1f;
            }
            else
            {
                globalaxis.x = 1f;
            }
        }

        List<GameObject> listGos = new List<GameObject>();

        foreach (var selection in Selection.gameObjects)
        {
            Transform selectionTr = selection.transform;
            Vector3 newSpt = selection.transform.position;
            Vector3 bnds = selection.GetComponent<MeshRenderer>().bounds.size;
            float relevantBnds = bnds.x;

            if (selectionTr.rotation.eulerAngles.y > 0 && selectionTr.rotation.eulerAngles.y < 180)//just track rotation relative to global??
            {
                if (!doZ)
                {
                    relevantBnds = bnds.z;
                }
                else
                {
                    relevantBnds = bnds.x;
                }
            }
            else// if (selectionTr.rotation.eulerAngles.y > 0)
            {
                if (!doZ)
                {
                    relevantBnds = bnds.z;
                }
                else
                {
                    relevantBnds = bnds.x;
                }
            }


            newSpt += (globalaxis * relevantBnds);

            newSpt.x = (Mathf.RoundToInt((newSpt.x * 2f))) / 2f;
            newSpt.z = (Mathf.RoundToInt((newSpt.z * 2f))) / 2f;
            GameObject newEmpty = Instantiate(selection, newSpt, selectionTr.rotation);
            // newEmpty.name = selection.name + "1";
            listGos.Add(newEmpty);

            string curNm = selection.name;
            string newNm = "";
            char[] charNm = curNm.ToCharArray();

            if (System.Char.IsDigit(charNm[charNm.Length - 1]))
            {
                int num = int.Parse(charNm[charNm.Length - 1].ToString());

                num++;
                charNm[charNm.Length - 1] = num.ToString().ToCharArray()[0];
            }

            for (int i = 0; i < charNm.Length; i++)
            {
                newNm += charNm[i];
            }

            newEmpty.name = newNm;
            Undo.RegisterCreatedObjectUndo(newEmpty, "dupup");
        }

        UnityEngine.Object[] finalSel = new UnityEngine.Object[Selection.objects.Length];

        for (int i = 0; i < listGos.Count; i++)
        {
            finalSel[i] = listGos[i];
        }


        EditorGUIUtility.PingObject(Selection.objects[0]);
        Selection.objects = finalSel;

        if (bringCam)
        {
            SceneView.lastActiveSceneView.pivot = Selection.transforms[0].position - relativCamPos;
            SceneView.lastActiveSceneView.Repaint();
        }
    }

    static void DupSnapDown(bool bringCam)
    {
        Vector3 relativCamPos = Selection.gameObjects[0].transform.position - SceneView.lastActiveSceneView.pivot;
        Vector3 fwd = SceneView.lastActiveSceneView.camera.transform.forward;
        Vector3 globalaxis = Vector3.zero;


        bool doZ = false;

        if (Mathf.Abs(fwd.z) > Mathf.Abs(fwd.x))//only compare x and z?
        {
            if (fwd.z < 0)
            {
                globalaxis.z = 1f;
            }
            else
            {
                globalaxis.z = -1f;
            }
        }
        else
        {
            if (fwd.x < 0)
            {
                globalaxis.x = 1f;
            }
            else
            {
                globalaxis.x = -1f;
            }
        }


        List<GameObject> listGos = new List<GameObject>();

        foreach (var selection in Selection.gameObjects)
        {
            Transform selectionTr = selection.transform;
            Vector3 newSpt = selection.transform.position;
            Vector3 bnds = selection.GetComponent<MeshRenderer>().bounds.size;
            float relevantBnds = bnds.x;

            if (selectionTr.rotation.eulerAngles.y > 0 && selectionTr.rotation.eulerAngles.y < 180)//just track rotation relative to global??
            {
                if (!doZ)
                {
                    relevantBnds = bnds.x;
                }
                else
                {
                    relevantBnds = bnds.z;
                }
            }
            else// if (selectionTr.rotation.eulerAngles.y > 0)
            {
                if (!doZ)
                {
                    relevantBnds = bnds.x;
                }
                else
                {
                    relevantBnds = bnds.z;
                }
            }

            newSpt += (globalaxis * relevantBnds);
            newSpt.x = (Mathf.RoundToInt((newSpt.x * 2f))) / 2f;
            newSpt.z = (Mathf.RoundToInt((newSpt.z * 2f))) / 2f;
            GameObject newEmpty = Instantiate(selection, newSpt, selectionTr.rotation);
            // newEmpty.name = selection.name + "1";
            listGos.Add(newEmpty);

            string curNm = selection.name;
            string newNm = "";
            char[] charNm = curNm.ToCharArray();

            if (System.Char.IsDigit(charNm[charNm.Length - 1]))
            {
                int num = int.Parse(charNm[charNm.Length - 1].ToString());

                num++;
                charNm[charNm.Length - 1] = num.ToString().ToCharArray()[0];
            }

            for (int i = 0; i < charNm.Length; i++)
            {
                newNm += charNm[i];
            }

            newEmpty.name = newNm;

            Undo.RegisterCreatedObjectUndo(newEmpty, "dupdown");
        }

        UnityEngine.Object[] finalSel = new UnityEngine.Object[Selection.objects.Length];

        for (int i = 0; i < listGos.Count; i++)
        {
            finalSel[i] = listGos[i];
        }


        EditorGUIUtility.PingObject(Selection.objects[0]);
        Selection.objects = finalSel;

        if (bringCam)
        {
            SceneView.lastActiveSceneView.pivot = Selection.transforms[0].position - relativCamPos;
            SceneView.lastActiveSceneView.Repaint();
        }

    }


    static void DupSnapRight(bool bringCam)
    {
        Vector3 relativCamPos = Selection.gameObjects[0].transform.position - SceneView.lastActiveSceneView.pivot;

        Vector3 fwd = SceneView.lastActiveSceneView.camera.transform.forward;
        Vector3 globalaxis = Vector3.zero;

        bool doZ = false;

        if (Mathf.Abs(fwd.z) > Mathf.Abs(fwd.x))//only compare x and z?
        {
            if (fwd.z < 0)
            {
                globalaxis.x = -1f;
            }
            else
            {
                globalaxis.x = 1f;
            }
        }
        else
        {
            doZ = true;

            if (fwd.x < 0)
            {
                globalaxis.z = 1f;
            }
            else
            {
                globalaxis.z = -1f;
            }
        }


        List<GameObject> listGos = new List<GameObject>();

        foreach (var selection in Selection.gameObjects)
        {
            Transform selectionTr = selection.transform;
            Vector3 newSpt = selection.transform.position;
            Vector3 bnds = selection.GetComponent<MeshRenderer>().bounds.size;


            float relevantBnds = bnds.x;

            // Debug.Log(selectionTr.rotation.eulerAngles.y);

            if (selectionTr.rotation.eulerAngles.y > 0 && selectionTr.rotation.eulerAngles.y < 180)//just track rotation relative to global??
            {
                if (!doZ)
                {
                    relevantBnds = bnds.x;
                }
                else
                {
                    relevantBnds = bnds.z;
                }
            }
            else// if (selectionTr.rotation.eulerAngles.y > 0)
            {
                if (!doZ)
                {
                    relevantBnds = bnds.x;
                }
                else
                {
                    relevantBnds = bnds.z;
                }
            }



            newSpt += (globalaxis * relevantBnds);
            newSpt.x = (Mathf.RoundToInt((newSpt.x * 2f))) / 2f;
            newSpt.z = (Mathf.RoundToInt((newSpt.z * 2f))) / 2f;
            GameObject newEmpty = Instantiate(selection, newSpt, selectionTr.rotation);
            // newEmpty.name = selection.name + "1";
            listGos.Add(newEmpty);

            string curNm = selection.name;
            string newNm = "";
            char[] charNm = curNm.ToCharArray();

            if (System.Char.IsDigit(charNm[charNm.Length - 1]))
            {
                int num = int.Parse(charNm[charNm.Length - 1].ToString());

                num++;
                charNm[charNm.Length - 1] = num.ToString().ToCharArray()[0];
            }

            for (int i = 0; i < charNm.Length; i++)
            {
                newNm += charNm[i];
            }

            newEmpty.name = newNm;
            Undo.RegisterCreatedObjectUndo(newEmpty, "dupright");

        }

        UnityEngine.Object[] finalSel = new UnityEngine.Object[Selection.objects.Length];

        for (int i = 0; i < listGos.Count; i++)
        {
            finalSel[i] = listGos[i];
        }


        EditorGUIUtility.PingObject(Selection.objects[0]);
        Selection.objects = finalSel;

        if (bringCam)
        {
            SceneView.lastActiveSceneView.pivot = Selection.transforms[0].position - relativCamPos;
            SceneView.lastActiveSceneView.Repaint();
        }
    }


    static void DupSnapLeft(bool bringCam)
    {
        Vector3 relativCamPos = Selection.gameObjects[0].transform.position - SceneView.lastActiveSceneView.pivot;
        Vector3 fwd = SceneView.lastActiveSceneView.camera.transform.forward;
        Vector3 globalaxis = Vector3.zero;

        bool doZ = false;
        if (Mathf.Abs(fwd.z) > Mathf.Abs(fwd.x))//only compare x and z?
        {
            if (fwd.z < 0)
            {
                globalaxis.x = 1f;
            }
            else
            {
                globalaxis.x = -1f;
            }
        }
        else
        {

            doZ = true;

            if (fwd.x < 0)
            {
                globalaxis.z = -1f;
            }
            else
            {
                globalaxis.z = 1f;
            }
        }

        List<GameObject> listGos = new List<GameObject>();

        foreach (var selection in Selection.gameObjects)
        {
            Transform selectionTr = selection.transform;
            Vector3 newSpt = selection.transform.position;
            Vector3 bnds = selection.GetComponent<MeshRenderer>().bounds.size;



            float relevantBnds = bnds.x;

            Debug.Log(selectionTr.rotation.eulerAngles.y);

            if (selectionTr.rotation.eulerAngles.y > 0 && selectionTr.rotation.eulerAngles.y < 180)//just track rotation relative to global??
            {
                if (!doZ)
                {
                    relevantBnds = bnds.x;
                }
                else
                {
                    relevantBnds = bnds.z;
                }
            }
            else// if (selectionTr.rotation.eulerAngles.y > 0)
            {
                if (!doZ)
                {
                    relevantBnds = bnds.x;
                }
                else
                {
                    relevantBnds = bnds.z;
                }
            }

            newSpt += (globalaxis * relevantBnds);
            newSpt.x = (Mathf.RoundToInt((newSpt.x * 2f))) / 2f;
            newSpt.z = (Mathf.RoundToInt((newSpt.z * 2f))) / 2f;
            GameObject newEmpty = Instantiate(selection, newSpt, selectionTr.rotation);
            // newEmpty.name = selection.name + "1";
            listGos.Add(newEmpty);

            string curNm = selection.name;
            string newNm = "";
            char[] charNm = curNm.ToCharArray();

            if (System.Char.IsDigit(charNm[charNm.Length - 1]))
            {
                int num = int.Parse(charNm[charNm.Length - 1].ToString());

                num++;
                charNm[charNm.Length - 1] = num.ToString().ToCharArray()[0];
            }

            for (int i = 0; i < charNm.Length; i++)
            {
                newNm += charNm[i];
            }

            newEmpty.name = newNm;


            EditorGUIUtility.PingObject(newEmpty);
            Undo.RegisterCreatedObjectUndo(newEmpty, "dupleft");

        }

        UnityEngine.Object[] finalSel = new UnityEngine.Object[Selection.objects.Length];

        for (int i = 0; i < listGos.Count; i++)
        {
            finalSel[i] = listGos[i];
        }


        EditorGUIUtility.PingObject(Selection.objects[0]);
        Selection.objects = finalSel;

        if (bringCam)
        {
            SceneView.lastActiveSceneView.pivot = Selection.transforms[0].position - relativCamPos;
            SceneView.lastActiveSceneView.Repaint();
        }
    }

    [MenuItem("DDemons/Cycle Swappable")]
    static void CycleSwap()
    {
        Swapite sp = (Selection.gameObjects[0]).GetComponent<Swapite>();
        if (sp == null)
        {
            return;
        }
        if (!swaps)
        {
            swaps = GameObject.FindGameObjectWithTag("Swappables").GetComponent<Swappables>();
        }


        if (swaps == null)
        {
            Debug.Log("no swaps");
        }
        else
        {

            if (!swaps.isSwapping)
            {
                // swaps.tempG = Selection.gameObjects[0];
                swaps.isSwapping = true;
            }
            else
            {
                swaps.isSwapping = false;
            }

        }
    }



    [MenuItem("DDemons/Select Fav Ctrl 1")]
    static void MakeFavoriteOne()
    {
        MakeCallSelect(0);
    }
    [MenuItem("DDemons/Select Fav Ctrl 2")]
    static void MakeFavoriteTwo()
    {
        MakeCallSelect(1);
    }
    [MenuItem("DDemons/Select Fav Ctrl 3")]
    static void MakeFavoriteThree()
    {
        MakeCallSelect(2);
    }
    [MenuItem("DDemons/Select Fav Ctrl 4")]
    static void MakeFavoriteFour()
    {
        MakeCallSelect(3);
    }
    [MenuItem("DDemons/Select Fav Ctrl 5")]
    static void MakeFavoriteFive()
    {
        MakeCallSelect(4);
    }

    [MenuItem("DDemons/Select Fav 1")]
    static void SelectFavoriteOne()
    {
        CallSelect(0);
    }
    [MenuItem("DDemons/Select Fav 2")]
    static void SelectFavoriteTwo()
    {
        CallSelect(1);
    }
    [MenuItem("DDemons/Select Fav 3")]
    static void SelectFavoriteThree()
    {
        CallSelect(2);
    }
    [MenuItem("DDemons/Select Fav 4")]
    static void SelectFavoriteFour()
    {
        CallSelect(3);
    }
    [MenuItem("DDemons/Select Fav 5")]
    static void SelectFavoriteFive()
    {
        CallSelect(4);
    }

    static void MakeCallSelect(int i)
    {
        if (!faves)
        {
            faves = GameObject.FindGameObjectWithTag("Favorites").GetComponent<Favorites>();
        }

        if (faves == null)
        {
            Debug.Log("no faves");
        }
        else
        {
            if (faves.fs != null)
            {
                if (i < faves.fs.Count)
                {

                    faves.fs[i] = ((GameObject)Selection.activeObject).transform;
                    EditorGUIUtility.PingObject(faves.fs[i]);

                }
                else
                {
                    Debug.Log("Add more items to favorites list");
                    EditorGUIUtility.PingObject(faves.transform);
                }
            }
        }
    }


    static void CallSelect(int i)
    {

        if (!faves)
        {
            GameObject faveFind = GameObject.FindGameObjectWithTag("Favorites");

            if (faveFind != null)
            {
                faves = faveFind.GetComponent<Favorites>();
            }
        }

        if (faves == null)
        {
            Debug.Log("no faves");
        }
        else
        {
            if (faves.fs != null)
            {
                if (i < faves.fs.Count)
                {

                    if (faves.fs[i] != null)
                    {
                        if (Selection.activeObject != null)
                        {
                            if (Selection.activeObject == faves.fs[i])
                            {
                                SceneView.lastActiveSceneView.FrameSelected();

                            }
                            else
                            {
                                Selection.activeObject = faves.fs[i];
                                EditorGUIUtility.PingObject(faves.fs[i]);
                            }
                        }
                        else
                        {
                            Selection.activeObject = faves.fs[i];
                            EditorGUIUtility.PingObject(faves.fs[i]);
                        }


                        // if (faves.lastSel != null)
                        // {

                        //     if (faves.lastSel == ((Transform)Selection.activeObject).gameObject)
                        //     {
                        //         // SceneView.lastActiveSceneView.FrameSelected();
                        //     }
                        // }

                        // faves.lastSel = ((Transform)Selection.activeObject).gameObject;
                    }
                }

            }
        }

    }


    [MenuItem("DDemons/Show Outline &1")]
    static void ToggleSelectOutline()
    {

        UnityEngine.Object[] selSet = Selection.objects;
        toggleRendo = !toggleRendo;

        foreach (GameObject obj in Selection.gameObjects)
        {
            Renderer rend = obj.GetComponent<Renderer>();

            if (rend)
            {


                if (toggleRendo)
                {
                    EditorUtility.SetSelectedRenderState(rend, EditorSelectedRenderState.Hidden);
                }
                else
                {
                    EditorUtility.SetSelectedRenderState(rend, EditorSelectedRenderState.Highlight);
                }

            }

            EditorUtility.SetDirty(obj);



        }

    }


    [MenuItem("DDemons/Delete All Colliders &4")]
    static void DeleteAllColliders()
    {

        UnityEngine.Object[] selSet = Selection.objects;
        toggleRendo = !toggleRendo;

        foreach (GameObject obj in Selection.gameObjects)
        {
            Collider col = obj.GetComponent<Collider>();

            if (col != null)
            {
                DestroyImmediate(col);

            }

        }


    }

    [MenuItem("DDemons/Scene Select &s")]
    static void SceneSelect()
    {
        UnityEngine.Object sceneAsset = AssetDatabase.LoadAssetAtPath("Assets/Scenes/0Home.unity", typeof(SceneAsset));
        ProjectWindowUtil.ShowCreatedAsset(sceneAsset);


    }

    [MenuItem("DDemons/Model Select &m")]
    static void ModelSelect()
    {
        UnityEngine.Object modelAsset = AssetDatabase.LoadAssetAtPath("Assets/Models/Model.mat", typeof(Material));
        ProjectWindowUtil.ShowCreatedAsset(modelAsset);


    }

}