using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class iPair
{
    public int a;
    public float val;
}

[System.Serializable]
public class Groundies
{
    public Transform sGround;
    public Transform wood;//cast to component later
    public Transform stone;
    public Transform food;

    public Transform enemySpawner;
    public Transform chest;
}

[System.Serializable]
public class Percentos
{
    public float noneAmt = .5f;
    public float woodAmt = .2f;
    public float stoneAmt = .1f;
    public float fishAmt = .08f;
    public float enemyAmt = .02f;
    public float chestAmt = .005f;
}
[System.Serializable]
public class RunningTotals
{
    public int woods;
    public int stones;
    public int foods;
    public int enemies;
    public int chest;
}

public class GenerateMap : MonoBehaviour
{
    //probably do in editor
    public AllAiManager allAis;
    public Groundies g;

    public List<Transform> map = new List<Transform>();

    public int size;//square map

    public Transform beginSpot;//automatically place? relative to zero

    public Transform buildMapParent;

    public Percentos percents;
    public RunningTotals totals;

    public List<iPair> testRolls = new List<iPair>();

    public Transform player;
    public PlayerChar plyrRef;
    public Preinteract preRef;

    public Vector3 buildPos;
    public float ringSize;
    public float ringThick;

    public List<AllAiManager> aiMan;

    void Start()
    {
        GenMapTime();
    }

    void GenMapTime()
    {
        allAis.eSpawnSpots.Clear();


        for (int i = 0; i < map.Count; i++)
        {
            DestroyImmediate(map[i].gameObject);

        }

        map.Clear();

        beginSpot.position = new Vector3(-size * 2f, 0f, -size * 2f);//* .5f
        buildPos = beginSpot.position;

        int row = 0;
        int column = 0;

        for (int i = 0; i < size; i++)
        {


            for (int r = 0; r < size; r++)
            {
                Transform newGen = null;
                Transform newGen2 = null;

                int randoGen = RandomToMapCreate();

                if ((buildPos - player.position).magnitude < 15f)
                {
                    randoGen = 0;
                }

                if (Mathf.Abs((buildPos - player.position).magnitude - ringSize) < ringThick)
                {
                    randoGen = 1;
                }

                //create ring?

                if (i == 0 || i == 1 || i == size - 1 || i == size - 2 || r == 0 || r == 1 || r == size - 1 || r == size - 2)
                {
                    randoGen = 1;
                }

                if (randoGen == 4)
                {
                    newGen = Instantiate(g.enemySpawner, buildPos, Quaternion.identity);
                    allAis.eSpawnSpots.Add(newGen.GetComponent<EnemySpawnSpot>());

                }
                else if (randoGen == 3)
                {
                    newGen = Instantiate(g.food, buildPos, Quaternion.identity);

                    WoodParent wp = newGen.GetComponent<WoodParent>();
                    if (wp != null)
                    {
                        wp.c.pre = preRef;
                        wp.c.plyr = plyrRef;
                        wp.sets[0].SetActive(false);
                        wp.sets[Random.Range(0, 2)].SetActive(true);
                    }


                }
                else
                {
                    newGen = Instantiate(g.sGround, buildPos, Quaternion.identity);
                }



                //Decide whether to place fishing or enemy spawn
                newGen.parent = buildMapParent;
                map.Add(newGen);

                //Decide whether to place tree or rock on top of ground
                switch (randoGen)
                {
                    case 1://wood
                        {
                            newGen2 = Instantiate(g.wood, buildPos, Quaternion.identity);
                            newGen2.parent = buildMapParent;
                            map.Add(newGen2);

                            WoodParent wp = newGen2.GetComponent<WoodParent>();
                            if (wp != null)
                            {
                                wp.c.pre = preRef;
                                wp.c.plyr = plyrRef;
                                wp.sets[0].SetActive(false);

                                int into = Random.Range(0, 3);

                                wp.sets[into].SetActive(true);
                                wp.sets[into].transform.parent = wp.transform;
                                wp.c.animo = wp.anims[into];
                                wp.activAnim = into;
                            }
                            break;
                        }
                    case 2://stone
                        {
                            newGen2 = Instantiate(g.stone, buildPos, Quaternion.identity);
                            newGen2.parent = buildMapParent;
                            map.Add(newGen2);

                            WoodParent wp = newGen2.GetComponent<WoodParent>();
                            if (wp != null)
                            {
                                wp.c.pre = preRef;
                                wp.c.plyr = plyrRef;
                                wp.sets[0].SetActive(false);
                                wp.sets[Random.Range(0, 2)].SetActive(true);

                            }
                            break;
                        }
                    case 5://chest
                        {
                            newGen2 = Instantiate(g.chest, buildPos, Quaternion.identity);
                            newGen2.parent = buildMapParent;
                            map.Add(newGen2);
                            break;
                        }
                }


                buildPos += new Vector3(4f, 0, 0f);
            }

            row++;
            buildPos += new Vector3(0, 0, 4f);
            buildPos.x = beginSpot.position.x;
        }
    }

    int RandomToMapCreate()
    {
        int given = 0;

        // float rollNone =
        List<iPair> rolls = new List<iPair>();
        //  testRolls = new List<iPair>();

        for (int i = 0; i < 6; i++)
        {
            iPair newRoll = new iPair
            {
                a = i,
                val = 0f,
            };

            switch (i)
            {
                case 0: //0 = simple
                    {
                        newRoll.val = 50f;
                        break;
                    }
                case 1: //1 = wood
                    {
                        newRoll.val = Random.Range(0f, 100f);

                        if (Random.Range(0f, 1f) > percents.woodAmt)
                        {
                            newRoll.val = 0f;
                        }
                        break;
                    }
                case 2: //2 = stone
                    {
                        newRoll.val = Random.Range(0f, 100f);

                        if (Random.Range(0f, 1f) > percents.stoneAmt)
                        {
                            newRoll.val = 0f;
                        }
                        break;
                    }
                case 3: //3 = fish
                    {
                        newRoll.val = Random.Range(0f, 100f);

                        if (Random.Range(0f, 1f) > percents.fishAmt)
                        {
                            newRoll.val = 0f;
                        }
                        break;
                    }
                case 4: //4 = enemy
                    {
                        newRoll.val = Random.Range(0f, 100f);

                        float distMult = ((buildPos - player.position).magnitude - 50f) / 200f;

                        float beatRandom = Mathf.Lerp(0f, .05f, Mathf.Clamp(distMult, 0f, 1f));

                        if (Random.Range(0f, 1f) > percents.enemyAmt + beatRandom)
                        {
                            newRoll.val = 0f;
                        }
                        break;
                    }
                case 5: //5 = chest
                    {
                        newRoll.val = Random.Range(0f, 100f);


                        float distMult = ((buildPos - player.position).magnitude - 50f) / 200f;

                        float beatRandom = Mathf.Lerp(0f, .02f, Mathf.Clamp(distMult, 0f, 1f));


                        if (Random.Range(0f, 1f) > percents.chestAmt + beatRandom)
                        {
                            newRoll.val = 0f;
                        }

                        break;
                    }
            }

            //  testRolls.Add(newRoll);
            rolls.Add(newRoll);
        }

        //  testRolls.Sort((a, b) => a.val.CompareTo(b.val));
        rolls.Sort((a, b) => a.val.CompareTo(b.val));


        //0 = simple
        //1 = wood
        //2 = stone
        //3 = fish
        //4 = enemy
        //5 = chest
        //  totals.stones
        given = rolls[rolls.Count - 1].a;



        return given;
    }
}
