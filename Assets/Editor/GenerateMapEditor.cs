using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GenerateMap))]
public class GenerateMapEditor : Editor
{
    public GenerateMap gen;
    // Start is called before the first frame update
    public override void OnInspectorGUI()
    {
        gen = (GenerateMap)target;

        // EditorGUILayout.Slider(cs.curMac, 0.0f, 5.0f, GUILayout.Width(300)));


        if (GUILayout.Button("MakeMap", GUILayout.MinWidth(100)))
        {
            GenMapTime();
        }

        if (GUILayout.Button("ClearMap", GUILayout.MinWidth(100)))
        {
            for (int i = 0; i < gen.map.Count; i++)
            {
                DestroyImmediate(gen.map[i].gameObject);

            }

            gen.allAis.eSpawnSpots.Clear();
            gen.map.Clear();

        }
        if (GUILayout.Button("TestRolls", GUILayout.MinWidth(100)))
        {
            int o = RandomToMapCreate();

        }

        DrawDefaultInspector();
    }

    void GenMapTime()
    {
        gen.allAis.eSpawnSpots.Clear();


        for (int i = 0; i < gen.map.Count; i++)
        {
            DestroyImmediate(gen.map[i].gameObject);

        }

        gen.map.Clear();

        gen.beginSpot.position = new Vector3(-gen.size * 2f, 0f, -gen.size * 2f);//* .5f
        gen.buildPos = gen.beginSpot.position;
        Vector3 buildPos = gen.beginSpot.position;

        int row = 0;
        int column = 0;

        for (int i = 0; i < gen.size; i++)
        {


            for (int r = 0; r < gen.size; r++)
            {
                Transform newGen = null;
                Transform newGen2 = null;

                int randoGen = RandomToMapCreate();

                if ((buildPos - gen.player.position).magnitude < 15f)
                {
                    randoGen = 0;
                }

                if (Mathf.Abs((buildPos - gen.player.position).magnitude - gen.ringSize) < gen.ringThick)
                {
                    randoGen = 1;
                }

                //create ring?

                if (i == 0 || i == 1 || i == gen.size - 1 || i == gen.size - 2 || r == 0 || r == 1 || r == gen.size - 1 || r == gen.size - 2)
                {
                    randoGen = 1;
                }

                if (randoGen == 4)
                {
                    newGen = Instantiate(gen.g.enemySpawner, buildPos, Quaternion.identity);
                    gen.allAis.eSpawnSpots.Add(newGen.GetComponent<EnemySpawnSpot>());

                }
                else if (randoGen == 3)
                {
                    newGen = Instantiate(gen.g.food, buildPos, Quaternion.identity);

                    WoodParent wp = newGen.GetComponent<WoodParent>();
                    if (wp != null)
                    {
                        wp.c.pre = gen.preRef;
                        wp.c.plyr = gen.plyrRef;
                        wp.sets[0].SetActive(false);
                        wp.sets[Random.Range(0, 2)].SetActive(true);
                    }


                }
                else
                {
                    newGen = Instantiate(gen.g.sGround, buildPos, Quaternion.identity);
                }



                //Decide whether to place fishing or enemy spawn
                newGen.parent = gen.buildMapParent;
                gen.map.Add(newGen);

                //Decide whether to place tree or rock on top of ground
                switch (randoGen)
                {
                    case 1://wood
                        {
                            newGen2 = Instantiate(gen.g.wood, buildPos, Quaternion.identity);
                            newGen2.parent = gen.buildMapParent;
                            gen.map.Add(newGen2);

                            WoodParent wp = newGen2.GetComponent<WoodParent>();
                            if (wp != null)
                            {
                                wp.c.pre = gen.preRef;
                                wp.c.plyr = gen.plyrRef;
                                wp.sets[0].SetActive(false);

                                int into = Random.Range(0, 3);

                                wp.sets[into].SetActive(true);
                                wp.sets[into].transform.parent = wp.transform;
                                wp.c.animo = wp.anims[into];
                                wp.activAnim = into;
                            }
                            break;
                        }
                    case 2://stone
                        {
                            newGen2 = Instantiate(gen.g.stone, buildPos, Quaternion.identity);
                            newGen2.parent = gen.buildMapParent;
                            gen.map.Add(newGen2);

                            WoodParent wp = newGen2.GetComponent<WoodParent>();
                            if (wp != null)
                            {
                                wp.c.pre = gen.preRef;
                                wp.c.plyr = gen.plyrRef;
                                wp.sets[0].SetActive(false);
                                wp.sets[Random.Range(0, 2)].SetActive(true);

                            }
                            break;
                        }
                    case 5://chest
                        {
                            newGen2 = Instantiate(gen.g.chest, buildPos, Quaternion.identity);
                            newGen2.parent = gen.buildMapParent;

                            Chest ch = newGen2.GetComponent<Chest>();
                            ch.p = gen.plyrRef;

                            gen.map.Add(newGen2);
                            break;
                        }
                }


                buildPos += new Vector3(4f, 0, 0f);
            }

            row++;
            buildPos += new Vector3(0, 0, 4f);
            buildPos.x = gen.beginSpot.position.x;
        }
    }

    int RandomToMapCreate()
    {
        int given = 0;

        // float rollNone =
        List<iPair> rolls = new List<iPair>();
        // gen.testRolls = new List<iPair>();

        for (int i = 0; i < 6; i++)
        {
            iPair newRoll = new iPair
            {
                a = i,
                val = 0f,
            };

            switch (i)
            {
                case 0: //0 = simple
                    {
                        newRoll.val = 50f;
                        break;
                    }
                case 1: //1 = wood
                    {
                        newRoll.val = Random.Range(0f, 100f);

                        if (Random.Range(0f, 1f) > gen.percents.woodAmt)
                        {
                            newRoll.val = 0f;
                        }
                        break;
                    }
                case 2: //2 = stone
                    {
                        newRoll.val = Random.Range(0f, 100f);

                        if (Random.Range(0f, 1f) > gen.percents.stoneAmt)
                        {
                            newRoll.val = 0f;
                        }
                        break;
                    }
                case 3: //3 = fish
                    {
                        newRoll.val = Random.Range(0f, 100f);

                        if (Random.Range(0f, 1f) > gen.percents.fishAmt)
                        {
                            newRoll.val = 0f;
                        }
                        break;
                    }
                case 4: //4 = enemy
                    {
                        newRoll.val = Random.Range(0f, 100f);

                        float distMult = ((gen.buildPos - gen.player.position).magnitude - 50f) / 200f;

                        float beatRandom = Mathf.Lerp(0f, .05f, Mathf.Clamp(distMult, 0f, 1f));

                        if (Random.Range(0f, 1f) > gen.percents.enemyAmt + beatRandom)
                        {
                            newRoll.val = 0f;
                        }
                        break;
                    }
                case 5: //5 = chest
                    {
                        newRoll.val = Random.Range(0f, 100f);


                        float distMult = ((gen.buildPos - gen.player.position).magnitude - 50f) / 200f;

                        float beatRandom = Mathf.Lerp(0f, .02f, Mathf.Clamp(distMult, 0f, 1f));


                        if (Random.Range(0f, 1f) > gen.percents.chestAmt + beatRandom)
                        {
                            newRoll.val = 0f;
                        }

                        break;
                    }
            }

            // gen.testRolls.Add(newRoll);
            rolls.Add(newRoll);
        }

        // gen.testRolls.Sort((a, b) => a.val.CompareTo(b.val));
        rolls.Sort((a, b) => a.val.CompareTo(b.val));


        //0 = simple
        //1 = wood
        //2 = stone
        //3 = fish
        //4 = enemy
        //5 = chest
        // gen.totals.stones
        given = rolls[rolls.Count - 1].a;



        return given;
    }
}
