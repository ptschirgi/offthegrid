﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Gradient_7Color" {
	Properties{
		// [PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Color 1(LEFT)", Color) = (1,1,1,1)
		_1Pos("1 Position", Range(0.001, 0.99)) = .01
		_Color2("Color 2", Color) = (1,1,1,1)
		_2Pos("2 Position", Range(0.001, 0.994)) = .15
		_Color3("Color 3", Color) = (1,1,1,1)
		_3Pos("3 Position", Range(0.001, 0.995)) = .3
		_Color4("Color 4", Color) = (1,1,1,1)
		_4Pos("4 Position", Range(0.001, 0.996)) = .45
		_Color5("Color 5(RIGHT)", Color) = (1,1,1,1)
		_5Pos("5 Position", Range(0.001, 0.997)) = .6
		_Color6("Color 6(RIGHT)", Color) = (1,1,1,1)
		_6Pos("6 Position", Range(0.001, 0.998)) = .75
		_Color7("Color 7(RIGHT)", Color) = (1,1,1,1)
		_7Pos("End Gradient", Range(0.001, 0.999)) = .99
	}

		SubShader{
		Tags{ "Queue" = "Background"  "IgnoreProjector" = "True" }
		LOD 100

		ZWrite On

		Pass{
		CGPROGRAM
#pragma vertex vert  
#pragma fragment frag
#include "UnityCG.cginc"

	fixed4 _Color;
	fixed4 _Color2;
	fixed4 _Color3;
	fixed4 _Color4;
	fixed4 _Color5;
	fixed4 _Color6;
	fixed4 _Color7;
	//float  _Middle;
	float  _1Pos;
	float  _2Pos;
	float  _3Pos;
	float  _4Pos;
	float  _5Pos; 
	float  _6Pos;
	float  _7Pos;

	struct v2f {
		float4 pos : SV_POSITION;
		float4 texcoord : TEXCOORD0;
	};

	v2f vert(appdata_full v) {
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.texcoord = v.texcoord;
		return o;
	}

	fixed4 frag(v2f i) : COLOR{
		
		fixed4 c = lerp(_Color7, _Color6, ((i.texcoord.x  ) / (1 - _6Pos)) ) * step((i.texcoord.x )  , (1 - _6Pos) );


	//
	c += lerp(_Color6, _Color5, (i.texcoord.x - (1 - _6Pos)) / (_6Pos - _5Pos)) * step((_5Pos), ((1 - i.texcoord.x) / _5Pos) - i.texcoord.x);
	c -= lerp(_Color6, _Color5, (i.texcoord.x - (1 - _6Pos)) / (_6Pos - _5Pos)) * step((_6Pos), ((1 - i.texcoord.x) / _6Pos) - i.texcoord.x);
	//
	//
	c += lerp(_Color5, _Color4, (i.texcoord.x - (1 - _5Pos)) / (_5Pos - _4Pos)) * step((_4Pos), ((1 - i.texcoord.x) / _4Pos) - i.texcoord.x);
	c -= lerp(_Color5, _Color4, (i.texcoord.x - (1 - _5Pos)) / (_5Pos - _4Pos)) * step((_5Pos), ((1 - i.texcoord.x) / _5Pos) - i.texcoord.x);
	//

				c += lerp(_Color4, _Color3, (i.texcoord.x - (1 - _4Pos)) / (_4Pos - _3Pos)) * step((_3Pos ) , ( (1 - i.texcoord.x) / _3Pos ) - i.texcoord.x);
				c -= lerp(_Color4, _Color3, (i.texcoord.x - (1 - _4Pos)) / (_4Pos - _3Pos)) * step((_4Pos), ((1 - i.texcoord.x) / _4Pos) - i.texcoord.x);

				c += lerp(_Color3, _Color2, (i.texcoord.x - (1 - _3Pos)) / (_3Pos - _2Pos)) * step((_2Pos), ((1 - i.texcoord.x) / _2Pos) - i.texcoord.x);
				c -= lerp(_Color3, _Color2, (i.texcoord.x - (1 - _3Pos)) / (_3Pos - _2Pos)) * step((_3Pos), ((1 - i.texcoord.x) / _3Pos) - i.texcoord.x);

				c += lerp(_Color2, _Color, (i.texcoord.x - (1 - _2Pos)) / (_2Pos - _1Pos)) * step((_1Pos), ((1 - i.texcoord.x) / _1Pos) - i.texcoord.x);
				c -= lerp(_Color2, _Color, (i.texcoord.x - (1 - _2Pos)) / (_2Pos - _1Pos)) * step((_2Pos), ((1 - i.texcoord.x) / _2Pos) - i.texcoord.x);

				c += _Color * step(((1 - i.texcoord.x) / _1Pos) - (i.texcoord.x + _1Pos), 0 );
				
	
	c.a = 1;
	return c;
	}
		ENDCG
	}
	}
}