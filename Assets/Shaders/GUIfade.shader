﻿Shader "Custom/GUIfade" {
Properties {
	_Color("Tint (A = Opacity)", Color) = (1,1,1,1)
	_MainTex ("Texture", 2D) = "white" {}
}
	
SubShader {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Tags { "LightMode" = "Vertex" }
	Cull Back
	Lighting Off

	Material {

	 }
	ColorMaterial AmbientAndDiffuse
	ZWrite Off
	//ColorMask RGB
	Blend SrcAlpha OneMinusSrcAlpha
	AlphaTest Greater 0
	Pass { 
		SetTexture [_MainTex]
		{
			ConstantColor[_Color]
			Combine texture * constant
			
		}
	}
}
}