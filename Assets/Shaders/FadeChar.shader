﻿Shader "Custom/FadedChar" {

		Properties{
			_MainTex("Base (RGB)", 2D) = "white" {}
		_BumpTex("BumpTex", 2D) = "bump" {}
		_Color1("Base Tint", Color) = (1, 1, 1, 1)
		}
			SubShader{
			LOD 200
			Tags{"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
			

			Pass {
				ZWrite Off//on?
				ColorMask 0
			}


			CGPROGRAM
#pragma surface surf Lambert  alpha

			sampler2D _MainTex, _BumpTex, _MaskTex;
		float4 _Color1, _Color2, _Color3;

		struct Input {
			float2 uv_MainTex;
			float2 uv_BumpTex;
			float2 uv_MaskTex;
		};

		void surf(Input IN, inout SurfaceOutput o) {
			half4 c = tex2D(_MainTex, IN.uv_MainTex);
			
			o.Albedo = c.rgb ;
			o.Normal = UnpackNormal(tex2D(_BumpTex, IN.uv_BumpTex));
			o.Alpha = _Color1.a;

		}


		ENDCG
		}
			FallBack "Diffuse"
	}