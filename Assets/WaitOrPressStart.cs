using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class WaitOrPressStart : MonoBehaviour
{

    public float autoTimer;
    public bool onceo;

    void Update()
    {
        // autoTimer += Time.deltaTime;

        // if (autoTimer > 4f)
        // {
        //     SceneManager.LoadScene("SampleScene");
        //     autoTimer = 0f;
        // }
        if (Input.GetAxis("Jump") > 0f)
        {
            if (!onceo)
            {
                onceo = true;
                SceneManager.LoadScene("SampleScene");
            }
        }
    }
}
