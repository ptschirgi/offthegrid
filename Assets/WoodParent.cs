using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodParent : MonoBehaviour
{
    public WoodCollect c;

    public List<GameObject> sets = new List<GameObject>();
    public List<GameObject> sets2 = new List<GameObject>();

    public int activAnim = -1;
    public List<Animator> anims = new List<Animator>();
}
