using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

[System.Serializable]
public class DayCounter
{
    public float time;//0 - 100 is whole day
    [Range(0f, 100f)]
    public float night = 70;//-100
    public bool afternoon;
    public bool warning;
    public bool switchToNight;


    //vis
    public Transform sunRot;
    public Light sunLight;
    public float daylightIntens = 0.52f;//-100
    public Transform beginRot;
    public Transform endDayRot;
}

[System.Serializable]
public class MusicSys
{
    public bool skipDayToLead;
    public bool skipLeadToNight;
    public bool skipNightToDay;
    public AudioSource day;
    public AudioSource leadup;
    public AudioSource night;

    public float baseVol = 1f;
}

[System.Serializable]
public class GameStats
{
    public int curDay = 1;
}

public class TimeOfDay : MonoBehaviour
{
    public AllAiManager allAis;
    public DayCounter day;
    public PlayerChar plyr;
    public MusicSys music;
    public float timeSpeed = 5f;

    public bool playFirstTimeLead;
    public bool playFirstTimeNight;

    public Transform rotTOD;
    public Transform rotStart;
    public Transform rotMid1;
    public Transform rotMid2;
    public Transform rotEnd;
    public float tnPercent;
    public float dayPercent;

    public MeshRenderer dayGradMsh;

    public TextMeshPro dayTitle;
    public TextMeshPro surivedScoreTxt;
    public TextMeshPro dayAnnounce;

    public GameStats gameStats;

    public Color daCol1;
    public Color daCol2;
    public GameObject gameOverScr;
    public bool gameOver;
    public List<float> nightPts;


    void Start()
    {
        StartCoroutine(FadeInDay());

        float percentTotalNight = 1f - ((100f - day.night) / 100f);

        float tenAdjTotalNight = 1f - ((100f - (day.night - 20f)) / 100f);


        day.sunRot.rotation = Quaternion.Slerp(day.beginRot.rotation, day.endDayRot.rotation, tnPercent);

        float threer = Mathf.Clamp(tenAdjTotalNight + .001f, .002f, .860f);
        float fourer = Mathf.Clamp(tenAdjTotalNight + .010f, .003f, .870f);

        dayGradMsh.materials[1].SetFloat("_2Pos", threer / 2f);
        dayGradMsh.materials[1].SetFloat("_3Pos", threer);
        dayGradMsh.materials[1].SetFloat("_4Pos", fourer);
    }


    public void PopEndScreen()
    {
        gameOverScr.SetActive(true);
        gameOver = true;

        surivedScoreTxt.text = "You survived to day " + gameStats.curDay.ToString();
    }

    void RestartGame()
    {
        SceneManager.LoadScene("SampleScene");
    }

    void Update()
    {
        if (gameOver)
        {
            if (Input.GetAxis("Jump") > 0f)
            {
                RestartGame();
            }
        }

        if (!gameOver)
        {

            day.time += Time.deltaTime * timeSpeed;
        }

        tnPercent = day.time / day.night;
        dayPercent = day.time / 100f;//total

        day.sunRot.rotation = Quaternion.Slerp(day.beginRot.rotation, day.endDayRot.rotation, tnPercent);
        // dayGradMsh.materials[1].SetFloat("_5Pos", Mathf.Clamp(fourer + .3f, 005f, .91f));


        if (dayPercent < .33f)
        {
            rotTOD.localRotation = Quaternion.Slerp(rotStart.localRotation, rotMid1.localRotation, dayPercent * 3f);
        }
        else if (dayPercent < .66f)
        {
            rotTOD.localRotation = Quaternion.Slerp(rotMid1.localRotation, rotMid2.localRotation, (dayPercent - .33f) * 3f);
        }
        else
        {
            rotTOD.localRotation = Quaternion.Slerp(rotMid2.localRotation, rotEnd.localRotation, (dayPercent - .66f) * 3f);
        }


        // music.day.volume = Mathf.Lerp(music.baseVol, 0f, Mathf.Clamp((tnPercent - .6f) * 2f, 0f, 1f));


        if (day.time >= day.night - 40f)
        {
            if (!day.afternoon)
            {
                music.skipNightToDay = true;
                StartCoroutine(FadeDayToLeadUp());
                day.afternoon = true;
            }
        }

        if (day.time >= day.night - 20f)
        {
            if (!day.warning)
            {
                StartCoroutine(FadeLeadupToNight());

                day.warning = true;
            }
        }

        if (day.time >= day.night)
        {
            if (!day.switchToNight)
            {
                StartCoroutine(FadeInNight());

                music.skipDayToLead = true;

                day.switchToNight = true;

            }

        }

        if (day.time >= 100f)
        {
            day.time = 0f;
            day.afternoon = false;
            day.warning = false;
            day.switchToNight = false;
            music.skipLeadToNight = true;
            StartCoroutine(FadeNightToDay());
            StartCoroutine(FadeInDay());




        }
    }


    IEnumerator FadeDayToLeadUp()
    {
        float t = 0f;
        music.skipDayToLead = false;
        bool skipLead = false;

        if (!playFirstTimeLead)
        {
            playFirstTimeLead = true;
            music.leadup.Play();
        }
        else
        {
            // skipLead = true;
        }

        while (t < 15f && !music.skipDayToLead)
        {
            t += Time.deltaTime;




            if (!skipLead)
            {
                music.leadup.volume = Mathf.Lerp(0f, music.baseVol, t * .1f);
            }
            else
            {
                music.leadup.volume = music.baseVol;
            }

            music.day.volume = Mathf.Lerp(music.baseVol, 0f, (t - 8f) * .4f);

            yield return null;
        }

        // music.day.volume = 0f;
    }


    IEnumerator FadeLeadupToNight()
    {
        float t = 0f;
        bool skipNight = false;
        bool switchone = false;
        music.skipLeadToNight = false;
        music.skipDayToLead = true;

        if (!playFirstTimeNight)
        {
            // playFirstTimeNight = true;
            music.night.Play();
            HardnessMode hm = HardnessMode.Easy;

            if (gameStats.curDay > 7)
            {
                hm = HardnessMode.Medium;
            }
            else if (gameStats.curDay > 3)
            {
                hm = HardnessMode.Hard;
            }

            allAis.NightTime(hm);

            // skipNight = true;
        }

        while (t < 2f && !music.skipLeadToNight)
        {
            t += Time.deltaTime;

            day.sunLight.intensity = Mathf.Lerp(day.daylightIntens, 0f, t);
            music.leadup.volume = Mathf.Lerp(music.baseVol, 0f, t * .8f);

            if (!skipNight)
            {
                music.night.volume = Mathf.Lerp(0f, music.baseVol, t);
            }
            else
            {
                music.night.volume = music.baseVol;
            }

            if (t >= 1f)
            {
                if (!switchone)
                {
                    plyr.SwitchTime(true);
                    switchone = true;
                    day.sunRot.gameObject.SetActive(false);
                }
            }

            yield return null;
        }

    }


    IEnumerator FadeNightToDay()
    {
        float t = 0f;
        music.skipNightToDay = false;

        while (t < 2f && !music.skipNightToDay)
        {
            t += Time.deltaTime;

            music.night.volume = Mathf.Lerp(music.baseVol, 0f, t);
            music.day.volume = Mathf.Lerp(0f, music.baseVol, t - .8f);

            yield return null;
        }
    }


    IEnumerator FadeInNight()
    {
        float t = 0f;

        while (t < 1f)
        {
            t += Time.deltaTime;

            // day.sunLight.shadowStrength = Mathf.Lerp(1f, 0f, t);


            yield return null;

        }


    }

    IEnumerator FadeInDay()
    {
        float t = 0f;
        // day.sunLight.shadowStrength = 0f;
        day.sunLight.intensity = 0f;
        day.sunRot.rotation = day.beginRot.rotation;
        day.sunRot.gameObject.SetActive(true);
        gameStats.curDay++;
        // day.night = 70f - (10f * (gameStats.curDay - .));
        if (gameStats.curDay <= nightPts.Count - 1)
        {
            day.night = nightPts[gameStats.curDay];
            allAis.speedMulto = 1f + (.03f * gameStats.curDay);
        }
        else
        {

        }

        dayTitle.text = gameStats.curDay.ToString();
        // dayTitle.text = "Day " + gameStats.curDay.ToString() + " - October " + gameStats.curDay.ToString() + ", 1997";
        dayAnnounce.text = "Day " + gameStats.curDay.ToString();




        float percentTotalNight = 1f - ((100f - day.night) / 100f);

        float tenAdjTotalNight = 1f - ((100f - (day.night - 20f)) / 100f);


        day.sunRot.rotation = Quaternion.Slerp(day.beginRot.rotation, day.endDayRot.rotation, tnPercent);

        float threer = Mathf.Clamp(tenAdjTotalNight + .001f, .002f, .860f);
        float fourer = Mathf.Clamp(tenAdjTotalNight + .010f, .003f, .870f);

        dayGradMsh.materials[1].SetFloat("_2Pos", threer / 2f);
        dayGradMsh.materials[1].SetFloat("_3Pos", threer);
        dayGradMsh.materials[1].SetFloat("_4Pos", fourer);





        HardnessMode hm = HardnessMode.Easy;

        if (gameStats.curDay > 7)
        {
            hm = HardnessMode.Medium;
        }
        else if (gameStats.curDay > 3)
        {
            hm = HardnessMode.Hard;
        }

        allAis.DayTime(hm);




        while (t < 1.4f)
        {
            t += Time.deltaTime;

            day.sunLight.intensity = Mathf.Lerp(0f, day.daylightIntens, t);

            dayAnnounce.color = Color.Lerp(daCol1, daCol2, t / 1.4f);
            // day.sunLight.shadowStrength = Mathf.Lerp(0f, 1f, t);

            yield return null;
        }

        plyr.SwitchTime(false);
    }

    void AnnounceNightime()
    {

    }
}
