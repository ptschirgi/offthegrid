using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodCollect : MonoBehaviour
{
    public InteractKind iKind;

    public int amt = 4;
    public PlayerChar plyr;
    public Preinteract pre;

    public GameObject activeObj;
    public GameObject deadObj;

    public Animator animo;

    public void PlyrTake()
    {
        amt--;

        if (amt <= 0)
        {
            ResourceDead();

            if (animo != null)
            {
                animo.SetBool("down", true);
            }
        }
        else
        {
            if (animo != null)
            {
                animo.SetTrigger("hit");
            }
        }

        plyr.Collect(iKind);



    }

    void ResourceDead()
    {
        pre.NoMoreX(iKind);
        deadObj.SetActive(true);
        activeObj.SetActive(false);

    }
}
