using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECM2;
using ECM2.Characters;
using ECM2.Examples.Gameplay.FirstPersonFlyingExample;
using TMPro;


[System.Serializable]
public class GatherResources
{
    public int wood;
    public int stone;
    public int food;

    //vis

    public TextMeshPro woodAmt;
    public TextMeshPro stoneAmt;
    public TextMeshPro foodAmt;

    //char vis


}

[System.Serializable]
public class FaceAnim
{
    public float fT;
    public SkinnedMeshRenderer sMsh;

    public int blink = 0;
    public int terrified = 1;
    public int dead = 2;
    public int happy = 3;
}

[System.Serializable]
public class BuildSys
{
    public int toolInt;

    public Transform emit;
    public Transform matchPt;

    public Color onCol;
    public Color offCol;

    public GameObject toolMenu;

    public bool menuOn;
    public bool focusOn;
    public float scrollTimer;
    public Transform scrollerino;
    // public float2 4
    public List<STool> sTools = new List<STool>();


    public BuildThing torch;
    public BuildThing holeCover;
    public BuildThing fire;

}

[System.Serializable]
public class ReqSet
{
    public int wood;
    public int stone;
    public int food;
}

[System.Serializable]
public enum AnimTooler
{
    None,//put away
    Axe,//put away
    Rod,//put away
    Pick,//put away
    Hole,//put away
    Torch,//put away
    Fire,//put away
}

[System.Serializable]
public class STool
{
    public Transform scl;
    public MeshRenderer bgMsh;
    public ReqSet req;

    public GameObject actTool;
    public GameObject subHovTool;

    public AnimTooler animTooler;
}

[System.Serializable]
public enum InteractKind
{
    None,
    Wood,
    Stone,
    Food,
    Hole,
    Torch,
    Fire
}

[System.Serializable]
public class PreVisTools
{


    //tools
    public GameObject toolParent;//because it auto selects
    public GameObject woodAxe;
    public GameObject stonePick;
    public GameObject foodRod;

    //tools
    public GameObject upgrade;
    public GameObject buildTorch;
    public GameObject coverHole;
    public GameObject buildFire;
}


[System.Serializable]
public class HpSys
{
    public bool isDead;
    public float cur = 100f;
    public Transform ext;

    public Transform cam;
    public Transform baseSpot;
    public Transform flyDeadSpot;

    public GameObject hpMsh;
}

[System.Serializable]
public class SkillGame
{
    //    -6.5f
    public Transform slider;
    public SkinnedMeshRenderer slideMsh;

    public AnimationCurve sCurv;
    public float sTime;

    public float minWindow = .4f;
    public float maxWindow = .6f;
}

[System.Serializable]
public class RegenAndPanic
{
    public bool regenActive;
    public bool panicActive;

    public float regenTimer;
    public float panicTimer;

    public ParticleSystem regen;
    public ParticleSystem panic;
}

public class PlayerChar : MonoBehaviour
{

    public TimeOfDay tod;
    public Preinteract preInteract;

    public Animator anim;
    public FaceAnim faceAnim;

    public MyFirstPersonCharacter fps;

    public GatherResources gather;

    public Transform plyrRot;


    public Transform camMover;
    public Transform camMatch;

    public InteractDetect interact;
    public bool clickPress;

    public Vector3 facingDir;

    public GameObject lantern;
    public Light l1;
    public Light l2;
    public Light l3;
    public Light l4;

    public InteractKind interactMODE;
    public BuildSys build;

    public PreVisTools preVis;

    public HpSys hp;

    public bool freezingAnimation;//stop movement
    public bool bigExpressions;

    public SkillGame skill;
    public GameObject deadTxt;

    public ReqSet holeCost;
    public ReqSet torchCost;
    public ReqSet fireCost;

    public float movSpeed = 8f;
    public float rotSpeed = 8f;

    public RegenAndPanic regenPanic;

    public Transform fwdRotation;
    public Transform bckRotation;
    public Transform fwdLeftRotation;
    public Transform fwdRightRotation;

    public Transform bckLeftRotation;
    public Transform bckRightRotation;
    // Start is called before the first frame update
    void Start()
    {
        build.toolMenu.SetActive(false);
        build.emit.parent = null;

        fps.hMult = movSpeed;
    }

    public void ChestHeal()
    {
        hp.cur = Mathf.Clamp(hp.cur + 15f, 0f, 100f);
        hp.ext.localPosition = new Vector3(-0.183f * (hp.cur / 100f), 0f, 0f);
    }

    public void CanRunAgain()
    {
        freezingAnimation = false;

        if (interactMODE == InteractKind.Wood || interactMODE == InteractKind.Food || interactMODE == InteractKind.Fire)
        {
            // anim.SetLayerWeight(1, 0f);

        }
    }


    public void SwitchTime(bool night)
    {
        if (night)
        {
            StartCoroutine(FadeInLantern());
        }
        else
        {
            lantern.SetActive(false);
        }
    }

    IEnumerator FadeInLantern()
    {
        float t = .2f;
        l1.intensity = .2f;
        l2.intensity = .2f;
        l3.intensity = .2f;
        l4.intensity = .2f;
        lantern.SetActive(true);
        while (t < 1f)
        {
            t += Time.deltaTime * 4f;

            l1.intensity = t;
            l2.intensity = t;
            l3.intensity = t;
            l4.intensity = t;

            yield return null;
        }

        l1.intensity = .92f;
        l2.intensity = .92f;
        l3.intensity = .92f;
        l4.intensity = .92f;
    }

    public void PreVisInteract(InteractKind iKind)
    {

        switch (iKind)
        {
            case InteractKind.None:
                {
                    // preVis.woodAxe.SetActive(false);
                    // preVis.stonePick.SetActive(false);
                    // preVis.foodRod.SetActive(false);
                    break;
                }
            case InteractKind.Wood:
                {
                    // preVis.woodAxe.SetActive(true);
                    // preVis.stonePick.SetActive(false);
                    // preVis.foodRod.SetActive(false);
                    break;
                }
            case InteractKind.Stone:
                {
                    // preVis.woodAxe.SetActive(false);
                    // preVis.stonePick.SetActive(true);
                    // preVis.foodRod.SetActive(false);
                    break;
                }
            case InteractKind.Food:
                {
                    // preVis.woodAxe.SetActive(false);
                    // preVis.stonePick.SetActive(false);
                    // preVis.foodRod.SetActive(true);
                    break;
                }
        }
    }

    public void Collect(InteractKind ik)
    {
        switch (ik)
        {
            case InteractKind.Wood:
                {
                    gather.wood += 1;
                    break;
                }
            case InteractKind.Stone:
                {
                    gather.stone += 1;
                    break;
                }
            case InteractKind.Food:
                {
                    gather.food += 1;
                    break;
                }
        }


        gather.woodAmt.text = gather.wood.ToString();
        gather.stoneAmt.text = gather.stone.ToString();
        gather.foodAmt.text = gather.food.ToString();
    }

    void OnTriggerEnter(Collider col)
    {
        if (!hp.isDead)
        {

            AtkZone az = col.GetComponent<AtkZone>();

            hp.cur -= az.dmg;

            hp.cur = Mathf.Clamp(hp.cur, 0f, 100f);
            hp.ext.localPosition = new Vector3(-0.183f * (hp.cur / 100f), 0f, 0f);

            if (hp.cur <= 0f)
            {
                hp.isDead = true;
                hp.hpMsh.SetActive(false);
                StartCoroutine(Dead());
            }

        }
    }

    IEnumerator Dead()
    {
        float t = 0f;
        anim.SetTrigger("dead");
        deadTxt.SetActive(true);
        // anim.SetLayerWeight(1, 0f);
        freezingAnimation = true;
        bigExpressions = true;
        faceAnim.sMsh.SetBlendShapeWeight(faceAnim.blink, 0f);
        faceAnim.sMsh.SetBlendShapeWeight(faceAnim.dead, 100f);
        bool onceo = false;
        while (t < 10f)
        {
            t += Time.deltaTime;
            if (!onceo && t > 4f)
                if (!onceo && t > 4f)
                {
                    onceo = true;
                    PopGameOverScreen();
                }

            hp.cam.localPosition = Vector3.Lerp(hp.baseSpot.localPosition, hp.flyDeadSpot.localPosition, t / 10f);

            yield return null;
        }


    }

    void PopGameOverScreen()
    {
        tod.PopEndScreen();
    }

    void FaceAnimations()
    {
        faceAnim.fT += Time.deltaTime;

        if (faceAnim.fT > 2f)
        {
            faceAnim.fT = 0f;

            if (!bigExpressions)
            {
                StartCoroutine(Blink());
            }
        }
    }

    IEnumerator Blink()
    {
        float t = 0f;
        faceAnim.sMsh.SetBlendShapeWeight(faceAnim.blink, 100f);

        while (t < .15f && !bigExpressions)
        {
            t += Time.deltaTime;


            yield return null;
        }

        faceAnim.sMsh.SetBlendShapeWeight(faceAnim.blink, 0f);
    }

    void SkillTime()
    {
        skill.sTime += Time.deltaTime;

        if (skill.sTime >= 1f)
        {
            skill.sTime = 0f;
        }

        skill.slider.localPosition = new Vector3(0f, 0f, skill.sCurv.Evaluate(skill.sTime) * -6.87f);

    }

    void Update()
    {
        if (!hp.isDead)
        {


            FaceAnimations();

            SkillTime();



            if (!freezingAnimation)
            {
                fps.fwd = Input.GetAxis("Vertical");
                // fps.lr = Input.GetAxis("Horizontal");

                if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
                {
                    facingDir.z = Input.GetAxis("Vertical");
                    facingDir.x = Input.GetAxis("Horizontal");
                    anim.SetFloat("fwd", 1f);

                    if (facingDir.z >= 0f)
                    {
                        if (facingDir.x > 0f)
                        {
                            plyrRot.localRotation = fwdRightRotation.localRotation;
                        }
                        else if (facingDir.x < 0f)
                        {
                            plyrRot.localRotation = fwdLeftRotation.localRotation;
                        }
                        else
                        {
                            plyrRot.localRotation = fwdRotation.localRotation;
                        }


                    }
                    else
                    {
                        if (facingDir.x > 0f)
                        {
                            plyrRot.localRotation = bckRightRotation.localRotation;
                        }
                        else if (facingDir.x < 0f)
                        {
                            plyrRot.localRotation = bckLeftRotation.localRotation;
                        }
                        else
                        {
                            plyrRot.localRotation = bckRotation.localRotation;
                        }

                    }
                }
                else
                {
                    fps.StopSprinting();
                    anim.SetFloat("fwd", 0f);

                }
            }
            else
            {
                fps.StopSprinting();
                fps.fwd = 0f;
                fps.lr = 0f;
                anim.SetFloat("fwd", 0f);
            }


            Vector3 fwd = this.transform.forward;


            if (facingDir != Vector3.zero)
            {

                // plyrRot.rotation = Quaternion.LookRotation(facingDir, Vector3.up);
            }
            if (!freezingAnimation)
            {
                float nega = 1f;

                if (facingDir.z < 0f)
                {
                    // nega = -1f;
                }
                this.transform.Rotate(new Vector3(0f, Input.GetAxis("Horizontal") * rotSpeed * nega, 0f));
            }

            // if (Input.GetAxis("Jump") > 0f)
            // {
            //     if (fps.CanJump())
            //     {

            //         fps.Jump();
            //     }
            // }
            // else
            // {
            //     fps.StopJumping();
            // }

            if (Input.GetMouseButton(0))
            {
                //interact
                if (!freezingAnimation)
                {

                    if (!clickPress)
                    {
                        clickPress = true;
                        ClickMouse();

                    }
                }
            }
            else
            {
                clickPress = false;
            }

            build.emit.position = new Vector3(SnappyClamp(build.matchPt.position.x), SnappyClamp(build.matchPt.position.y), SnappyClamp(build.matchPt.position.z));

            if (build.toolInt == 4)
            {

                if (preInteract.nearEnemySpawn != null)
                {
                    build.emit.position = preInteract.nearEnemySpawn.transform.position;
                }
            }

            if (build.menuOn)
            {
                build.scrollTimer += Time.deltaTime;

                if (build.scrollTimer > 1f)
                {
                    if (build.focusOn)
                    {

                        build.focusOn = false;
                        build.toolMenu.SetActive(false);
                        build.sTools[build.toolInt].subHovTool.SetActive(true);

                    }
                }
                if (build.scrollTimer > 1.75f)
                {
                    if (build.menuOn)
                    {
                        build.menuOn = false;
                        build.sTools[build.toolInt].subHovTool.SetActive(false);
                    }

                }
            }




            ToolScroll();


        }

    }

    void ClickMouse()
    {
        switch (build.toolInt)
        {
            case 0://none
                {
                    if (!bigExpressions)
                    {

                        StartCoroutine(DoEmote());
                    }
                    break;
                }
            case 1://AXE
                {
                    anim.SetTrigger("axeSwing");
                    freezingAnimation = true;
                    // anim.SetLayerWeight(1, 0f);


                    if (preInteract.nearWood != null)
                    {
                        preInteract.nearWood.PlyrTake();
                    }
                    break;
                }
            case 2://ROD
                {
                    anim.SetTrigger("rodSwing");
                    freezingAnimation = true;
                    // anim.SetLayerWeight(1, 0f);


                    if (preInteract.nearFood != null)
                    {
                        preInteract.nearFood.PlyrTake();
                    }

                    break;
                }
            case 3://PICK
                {
                    anim.SetTrigger("pickSwing");
                    freezingAnimation = true;
                    // anim.SetLayerWeight(1, 0f);


                    if (preInteract.nearStone != null)
                    {
                        preInteract.nearStone.PlyrTake();
                    }

                    break;
                }
            case 4://build HOLE
                {
                    if (preInteract.nearEnemySpawn != null)
                    {
                        if (MatchAndCost(holeCost))
                        {
                            BuildThing bt = Instantiate(build.holeCover, build.emit.position, Quaternion.identity);
                            preInteract.nearEnemySpawn.covered = true;
                            preInteract.nearEnemySpawn.covery = bt.gameObject;
                        }
                    }


                    break;
                }
            case 5://build TORCH
                {
                    if (MatchAndCost(torchCost))
                    {
                        Instantiate(build.torch, build.emit.position, Quaternion.identity);
                    }

                    break;
                }
            case 6://build fire
                {
                    if (MatchAndCost(fireCost))
                    {
                        Instantiate(build.fire, build.emit.position, Quaternion.identity);
                    }

                    break;
                }
        }


    }

    bool MatchAndCost(ReqSet rs)
    {
        bool good = false;

        Debug.Log(rs.stone.ToString() + rs.wood.ToString() + rs.food.ToString());
        Debug.Log(gather.stone.ToString() + gather.wood.ToString() + gather.food.ToString());


        if (gather.stone >= rs.stone)
        {
            Debug.Log("stone check");
            if (gather.wood >= rs.wood)
            {
                Debug.Log("wood check");

                if (gather.food >= rs.food)
                {
                    Debug.Log("food check");

                    good = true;
                }
            }
        }
        if (good)
        {
            gather.stone -= rs.stone;
            gather.wood -= rs.wood;
            gather.food -= rs.food;
            gather.woodAmt.text = gather.wood.ToString();
            gather.stoneAmt.text = gather.stone.ToString();
            gather.foodAmt.text = gather.food.ToString();
        }

        return good;
    }

    bool SimpleCost(ReqSet rs)
    {
        bool good = false;

        Debug.Log(rs.stone.ToString() + rs.wood.ToString() + rs.food.ToString());
        Debug.Log(gather.stone.ToString() + gather.wood.ToString() + gather.food.ToString());


        if (gather.stone >= rs.stone)
        {
            Debug.Log("stone check");
            if (gather.wood >= rs.wood)
            {
                Debug.Log("wood check");

                if (gather.food >= rs.food)
                {
                    Debug.Log("food check");

                    good = true;
                }
            }
        }


        return good;
    }

    void ToolScroll()
    {
        bool scrolling = true;
        int prevInt = build.toolInt;

        if (Input.GetAxis("Mouse ScrollWheel") == 0f)
        {
            scrolling = false;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            build.scrollTimer = 0f;
            if (!build.menuOn || !build.focusOn)
            {
                build.menuOn = true;
                build.focusOn = true;
                build.toolMenu.SetActive(true);

            }

            if (build.toolInt > 0)
            {
                build.toolInt--;
            }
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            build.scrollTimer = 0f;

            if (!build.menuOn || !build.focusOn)
            {
                build.menuOn = true;
                build.focusOn = true;
                build.toolMenu.SetActive(true);
            }

            if (build.toolInt < build.sTools.Count - 1)
            {
                build.toolInt++;
            }
        }

        if (scrolling)
        {
            for (int i = 0; i < build.sTools.Count; i++)
            {
                if (build.toolInt != i)
                {
                    build.sTools[i].scl.localScale = new Vector3(.75f, .75f, .75f);
                    build.sTools[i].bgMsh.material.SetColor("_Color", build.offCol);
                    build.sTools[i].subHovTool.SetActive(false);
                    build.sTools[i].actTool.SetActive(false);
                }

            }

            build.sTools[build.toolInt].scl.localScale = Vector3.one;
            build.sTools[build.toolInt].bgMsh.material.SetColor("_Color", build.onCol);
            build.sTools[build.toolInt].actTool.SetActive(true);



            if (build.toolInt == 5)
            {
                build.scrollerino.localPosition = new Vector3(0, 2f, 0);
            }
            else if (build.toolInt == 6)
            {
                build.scrollerino.localPosition = new Vector3(0, 4f, 0);
            }
            else
            {
                build.scrollerino.localPosition = Vector3.zero;
            }

        }

        if (prevInt != build.toolInt)
        {
            switch (build.toolInt)
            {
                case 0://NONE
                    {
                        preVis.woodAxe.SetActive(false);
                        preVis.foodRod.SetActive(false);
                        preVis.stonePick.SetActive(false);
                        preVis.toolParent.SetActive(true);
                        //set actual tool elsewhere

                        // preVis.upgrade.SetActive(false);
                        preVis.buildTorch.SetActive(false);
                        preVis.coverHole.SetActive(false);
                        preVis.buildFire.SetActive(false);

                        anim.SetTrigger("putAway");
                        freezingAnimation = true;
                        // anim.SetLayerWeight(1, 0f);

                        interactMODE = InteractKind.None;//pickup?

                        break;
                    }
                case 1://AXE
                    {
                        preVis.woodAxe.SetActive(true);
                        preVis.foodRod.SetActive(false);
                        preVis.stonePick.SetActive(false);

                        preVis.toolParent.SetActive(false);

                        // preVis.upgrade.SetActive(false);

                        preVis.buildTorch.SetActive(false);
                        preVis.coverHole.SetActive(false);
                        preVis.buildFire.SetActive(false);
                        anim.SetTrigger("takeOut");
                        freezingAnimation = true;
                        // anim.SetLayerWeight(1, 1f);

                        interactMODE = InteractKind.Wood;

                        break;
                    }
                case 2://ROD
                    {
                        preVis.woodAxe.SetActive(false);
                        preVis.foodRod.SetActive(true);
                        preVis.stonePick.SetActive(false);

                        preVis.toolParent.SetActive(false);
                        preVis.buildTorch.SetActive(false);

                        // preVis.upgrade.SetActive(false);
                        preVis.coverHole.SetActive(false);
                        preVis.buildFire.SetActive(false);
                        anim.SetTrigger("takeOut");
                        freezingAnimation = true;
                        // anim.SetLayerWeight(1, 1f);
                        interactMODE = InteractKind.Food;

                        break;
                    }
                case 3://PICK
                    {
                        preVis.woodAxe.SetActive(false);
                        preVis.foodRod.SetActive(false);
                        preVis.stonePick.SetActive(true);

                        preVis.toolParent.SetActive(false);
                        preVis.buildTorch.SetActive(false);

                        // preVis.upgrade.SetActive(false);
                        preVis.coverHole.SetActive(false);
                        preVis.buildFire.SetActive(false);
                        anim.SetTrigger("takeOut");

                        freezingAnimation = true;
                        // anim.SetLayerWeight(1, 1f);
                        interactMODE = InteractKind.Stone;

                        break;
                    }
                case 4://HOLE COVER
                    {

                        if (prevInt == 3)
                        {
                            anim.SetTrigger("putAway");

                            freezingAnimation = true;
                        }
                        preVis.woodAxe.SetActive(false);
                        preVis.foodRod.SetActive(false);
                        preVis.stonePick.SetActive(false);

                        preVis.toolParent.SetActive(false);
                        preVis.coverHole.SetActive(true);

                        // preVis.upgrade.SetActive(false);
                        preVis.buildTorch.SetActive(false);
                        preVis.buildFire.SetActive(false);
                        // anim.SetLayerWeight(1, 0f);
                        interactMODE = InteractKind.Hole;

                        if (SimpleCost(holeCost))
                        {
                            // "_Color" = GREEN
                        }
                        else
                        {

                        }

                        break;
                    }
                case 5://BUILD TORCH
                    {

                        // anim.SetLayerWeight(1, 0f);
                        preVis.toolParent.SetActive(false);
                        preVis.buildTorch.SetActive(true);

                        // preVis.upgrade.SetActive(false);
                        preVis.coverHole.SetActive(false);
                        preVis.buildFire.SetActive(false);
                        interactMODE = InteractKind.Torch;

                        break;
                    }
                case 6://BUILD FIRE
                    {
                        preVis.toolParent.SetActive(false);
                        preVis.buildFire.SetActive(true);

                        // preVis.upgrade.SetActive(false);
                        preVis.buildTorch.SetActive(false);
                        preVis.coverHole.SetActive(false);
                        // anim.SetLayerWeight(1, 0f);
                        interactMODE = InteractKind.Fire;

                        break;
                    }
            }

        }

    }

    float SnappyClamp(float raw)
    {
        float clmped = raw;

        clmped = Mathf.RoundToInt(clmped / 2f) * 2f;

        return clmped;
    }

    IEnumerator DoEmote()
    {
        float t = 0f;
        bigExpressions = true;
        faceAnim.sMsh.SetBlendShapeWeight(faceAnim.blink, 0f);
        if (tod.day.time <= tod.day.night)
        {
            faceAnim.sMsh.SetBlendShapeWeight(faceAnim.happy, 100f);

            if (!regenPanic.regenActive)
            {
                StartCoroutine(Regeno());
                regenPanic.regen.Play();
            }
            else
            {
                regenPanic.regenTimer = 0f;
                regenPanic.regen.Play();
            }

        }
        else
        {
            faceAnim.sMsh.SetBlendShapeWeight(faceAnim.terrified, 100f);

            if (!regenPanic.panicActive)
            {
                StartCoroutine(Panico());
                regenPanic.panic.Play();
            }
            else
            {
                regenPanic.regenTimer = 0f;
                regenPanic.panic.Play();
            }
        }





        while (t < .8f)
        {
            t += Time.deltaTime;

            yield return null;
        }

        faceAnim.sMsh.SetBlendShapeWeight(faceAnim.happy, 0f);
        faceAnim.sMsh.SetBlendShapeWeight(faceAnim.terrified, 0f);

        bigExpressions = false;


    }

    IEnumerator Regeno()
    {
        regenPanic.regenTimer = 0f;
        regenPanic.regenActive = true;

        while (regenPanic.regenTimer < 1f)
        {
            regenPanic.regenTimer += Time.deltaTime;

            hp.cur += Time.deltaTime * 5f;

            yield return null;
        }

        regenPanic.regenActive = false;
    }

    IEnumerator Panico()
    {
        regenPanic.panicTimer = 0f;
        regenPanic.panicActive = true;


        while (regenPanic.panicTimer < 1f)
        {
            regenPanic.panicTimer += Time.deltaTime;

            fps.hMult = movSpeed + 1.5f;

            yield return null;
        }

        regenPanic.panicActive = false;
        fps.hMult = movSpeed;
    }
    // IEnumerator InteractTiming()
    // {
    //     float t = 0f;
    //     // interact.gameObject.SetActive(true);
    //     if (preInteract.nearWood != null)
    //     {
    //         preInteract.nearWood.PlyrTake();
    //     }


    //     while (t < .1f)
    //     {
    //         t += Time.deltaTime;

    //         yield return null;
    //     }

    //     // interact.gameObject.SetActive(false);
    // }

    void LateUpdate()
    {
        camMover.position = Vector3.Lerp(camMover.position, camMatch.position, Time.deltaTime * 15f);
    }
}
