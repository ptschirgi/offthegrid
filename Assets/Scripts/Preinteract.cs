using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Preinteract : MonoBehaviour
{
    public PlayerChar plyr;

    public WoodCollect nearWood;
    public WoodCollect nearStone;
    public WoodCollect nearFood;
    public EnemySpawnSpot nearEnemySpawn;
    // public WoodCollect nearWood;
    // public WoodCollect nearWood;

    void OnTriggerEnter(Collider col)
    {
        WoodCollect wood = col.GetComponent<WoodCollect>();
        NearHoleBuild e = col.GetComponent<NearHoleBuild>();

        if (wood != null)
        {

            switch (wood.iKind)
            {
                case InteractKind.Wood:
                    {
                        plyr.PreVisInteract(wood.iKind);
                        nearWood = wood;
                        break;
                    }
                case InteractKind.Stone:
                    {
                        plyr.PreVisInteract(wood.iKind);
                        nearStone = wood;
                        break;
                    }
                case InteractKind.Food:
                    {
                        plyr.PreVisInteract(wood.iKind);
                        nearFood = wood;
                        break;
                    }
            }


        }
        if (e != null)
        {

            nearEnemySpawn = e.ess;

        }
        // if (wood != null)
        // {
        //     wood.PlyrTake();
        // }
        // if (wood != null)
        // {
        //     wood.PlyrTake();
        // }
    }
    void OnTriggerExit(Collider col)
    {
        WoodCollect wood = col.GetComponent<WoodCollect>();
        NearHoleBuild e = col.GetComponent<NearHoleBuild>();


        if (wood != null)
        {

            switch (wood.iKind)
            {
                case InteractKind.Wood:
                    {
                        if (nearWood == wood)
                        {
                            nearWood = null;
                        }
                        break;
                    }
                case InteractKind.Stone:
                    {
                        if (nearStone == wood)
                        {
                            nearStone = null;
                        }
                        break;
                    }
                case InteractKind.Food:
                    {
                        if (nearFood == wood)
                        {
                            nearFood = null;
                        }
                        break;
                    }
            }


        }

        if (e != null)
        {

            nearEnemySpawn = null;

        }
        // if (wood != null)
        // {
        //     wood.PlyrTake();
        // }
        // if (wood != null)
        // {
        //     wood.PlyrTake();
        // }

        OffIfOff();
    }

    public void NoMoreX(InteractKind ik)
    {
        switch (ik)
        {
            case InteractKind.Wood:
                {

                    nearWood = null;
                    break;
                }
            case InteractKind.Stone:
                {

                    nearStone = null;
                    break;
                }
            case InteractKind.Food:
                {

                    nearFood = null;
                    break;
                }
        }

        OffIfOff();

    }

    void OffIfOff()
    {
        if (nearWood == null && nearStone == null && nearFood == null)
        {
            plyr.PreVisInteract(InteractKind.None);
        }
    }
}
