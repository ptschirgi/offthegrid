using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Monster : MonoBehaviour
{
    public Transform plyr;
    public NavMeshAgent agent;

    public Animator anim;
    public Rigidbody rby;

    public bool canAtk = true;
    public float pDist;
    public float atkLength = 1f;
    public float triggerZone = .7f;
    public float cooldown = .8f;
    public GameObject atkHit;

    public bool spawned;
    public Transform spawnObj;
    public AnimationCurve jumpCurv;

    // Start is called before the first frame update
    void Start()
    {
        SpawnoMode();
    }

    public void SpawnoMode()
    {
        StartCoroutine(Spawny());

    }
    IEnumerator Spawny()
    {
        float t = 0f;
        agent.enabled = false;


        while (t < 1f)
        {
            t += Time.deltaTime;
            this.transform.position = spawnObj.position + new Vector3(0, jumpCurv.Evaluate(t), 0);

            yield return null;
        }

        spawned = true;
        agent.enabled = true;

    }

    void FixedUpdate()
    {
        if (spawned)
        {
            if (canAtk)
            {

                agent.SetDestination(plyr.position);
            }
            else
            {
                Quaternion lookRow = Quaternion.LookRotation(plyr.position - this.transform.position, Vector3.up);
                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, lookRow, Time.deltaTime * 3f);
                // this.transform.LookAt(plyr.position, Vector3.up);
            }
        }

    }


    void Update()
    {

        pDist = (plyr.position - this.transform.position).magnitude;

        if (canAtk)
        {
            if (anim != null)
            {
                anim.SetFloat("fwd", 1f);
            }
        }
        else
        {
            if (anim != null)
            {
                anim.SetFloat("fwd", 0f);
            }
        }

        if (pDist < 2.5f)
        {
            if (canAtk)
            {
                canAtk = false;
                StartCoroutine(Attako());
            }
        }
    }

    IEnumerator Attako()
    {
        float t = 0f;
        bool trig = false;
        bool offer = false;
        if (anim != null)
        {

            anim.SetTrigger("atk");
        }
        agent.isStopped = true;

        while (t < atkLength + cooldown)
        {
            t += Time.deltaTime;

            //play anim
            if (t > triggerZone)
            {
                if (!trig)
                {
                    trig = true;
                    atkHit.SetActive(true);

                }
            }

            if (t > triggerZone + .1f)
            {
                if (!offer)
                {
                    offer = true;
                    atkHit.SetActive(false);

                }
            }

            yield return null;
        }

        canAtk = true;
    }
}
