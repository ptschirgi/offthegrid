using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VrAttachMove : MonoBehaviour
{
    public Transform rby;
    public Transform vrRig;
    public Transform cam;

    public Vector3 prevLoco;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 loco = cam.position - vrRig.position;
        loco.y = 0f;
        prevLoco = loco;

        vrRig.position = rby.position - loco;//- loco
    }

    void FixedUpdate()
    {
        Debug.Log(cam.localPosition);
        Vector3 loco = cam.position - vrRig.position;
        loco.y = 0f;


        rby.position = rby.position + loco - prevLoco;
        vrRig.position = rby.position - loco;
        // vrRig.position = rby.position;
        prevLoco = loco;
    }
}
