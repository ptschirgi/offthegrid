using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UpTier
{
    public GameObject obj;
    public string effect;
}

public class BuildThing : MonoBehaviour
{
    public int curTier;

    public List<ReqSet> upCosts = new List<ReqSet>();

    public List<UpTier> upTiers = new List<UpTier>();

    public void Upgrade()
    {
        curTier++;

        for (int i = 0; i < upTiers.Count; i++)
        {

        }
    }
}
