%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: HandsUpper
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: b
    m_Weight: 0
  - m_Path: b/elbowTarget.L
    m_Weight: 0
  - m_Path: b/elbowTarget.L/elbowTarget.L_end
    m_Weight: 0
  - m_Path: b/elbowTarget.R
    m_Weight: 0
  - m_Path: b/elbowTarget.R/elbowTarget.R_end
    m_Weight: 0
  - m_Path: b/kneeTarget.L
    m_Weight: 0
  - m_Path: b/kneeTarget.L/kneeTarget.L_end
    m_Weight: 0
  - m_Path: b/kneeTarget.R
    m_Weight: 0
  - m_Path: b/kneeTarget.R/kneeTarget.R_end
    m_Weight: 0
  - m_Path: b/root
    m_Weight: 0
  - m_Path: b/root/toolControl
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.L
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.L/handControl.L_end
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003/fishingrod.004
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003/fishingrod.004/fishingrodFeathers
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003/fishingrod.004/fishingrodFeathers/fishingrodFeathers_end
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003/fishingrod.004/string1
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003/fishingrod.004/string1/string2
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003/fishingrod.004/string1/string2/string3
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003/fishingrod.004/string1/string2/string3/string4
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003/fishingrod.004/string1/string2/string3/string4/string5
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003/fishingrod.004/string1/string2/string3/string4/string5/string6
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003/fishingrod.004/string1/string2/string3/string4/string5/string6/string7
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003/fishingrod.004/string1/string2/string3/string4/string5/string6/string7/string8
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003/fishingrod.004/string1/string2/string3/string4/string5/string6/string7/string8/hook
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/fishingrod/fishingrod.001/fishingrod.002/fishingrod.003/fishingrod.004/string1/string2/string3/string4/string5/string6/string7/string8/hook/hook_end
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/tool
    m_Weight: 1
  - m_Path: b/root/toolControl/handControl.R/tool/tool_end
    m_Weight: 0
  - m_Path: b/spine
    m_Weight: 0
  - m_Path: b/spine/hip.L
    m_Weight: 0
  - m_Path: b/spine/hip.L/thigh.L
    m_Weight: 0
  - m_Path: b/spine/hip.L/thigh.L/shin.L
    m_Weight: 0
  - m_Path: b/spine/hip.L/thigh.L/shin.L/foot.L
    m_Weight: 0
  - m_Path: b/spine/hip.L/thigh.L/shin.L/foot.L/foot.L_end
    m_Weight: 0
  - m_Path: b/spine/hip.R
    m_Weight: 0
  - m_Path: b/spine/hip.R/thigh.R
    m_Weight: 0
  - m_Path: b/spine/hip.R/thigh.R/shin.R
    m_Weight: 0
  - m_Path: b/spine/hip.R/thigh.R/shin.R/foot.R
    m_Weight: 0
  - m_Path: b/spine/hip.R/thigh.R/shin.R/foot.R/foot.R_end
    m_Weight: 0
  - m_Path: b/spine/spine.001
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002
    m_Weight: 1
  - m_Path: b/spine/spine.001/spine.002/neck
    m_Weight: 1
  - m_Path: b/spine/spine.001/spine.002/neck/head
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/bangs1
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/bangs1/bangs1_end
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/bangs2
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/bangs2/bangs2_end
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/hairMain
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/hairMain/hairMain_end
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/head.001.L
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/head.001.L/head.001.L_end
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/head.001.R
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/head.001.R/head.001.R_end
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/pigtail.L
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/pigtail.L/pigtail.L_end
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/pigtail.R
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/neck/head/pigtail.R/pigtail.R_end
    m_Weight: 0
  - m_Path: b/spine/spine.001/spine.002/shoulder.L
    m_Weight: 1
  - m_Path: b/spine/spine.001/spine.002/shoulder.L/upperarm.L
    m_Weight: 1
  - m_Path: b/spine/spine.001/spine.002/shoulder.L/upperarm.L/forearm.L
    m_Weight: 1
  - m_Path: b/spine/spine.001/spine.002/shoulder.L/upperarm.L/forearm.L/hand.L
    m_Weight: 1
  - m_Path: b/spine/spine.001/spine.002/shoulder.L/upperarm.L/forearm.L/hand.L/fingers.L
    m_Weight: 1
  - m_Path: b/spine/spine.001/spine.002/shoulder.L/upperarm.L/forearm.L/hand.L/fingers.L/fingers.L_end
    m_Weight: 1
  - m_Path: b/spine/spine.001/spine.002/shoulder.R
    m_Weight: 1
  - m_Path: b/spine/spine.001/spine.002/shoulder.R/upperarm.R
    m_Weight: 1
  - m_Path: b/spine/spine.001/spine.002/shoulder.R/upperarm.R/forearm.R
    m_Weight: 1
  - m_Path: b/spine/spine.001/spine.002/shoulder.R/upperarm.R/forearm.R/hand.R
    m_Weight: 1
  - m_Path: b/spine/spine.001/spine.002/shoulder.R/upperarm.R/forearm.R/hand.R/fingers.R
    m_Weight: 1
  - m_Path: b/spine/spine.001/spine.002/shoulder.R/upperarm.R/forearm.R/hand.R/fingers.R/fingers.R_end
    m_Weight: 1
  - m_Path: b/toe.L
    m_Weight: 0
  - m_Path: b/toe.L/toePivot.L
    m_Weight: 0
  - m_Path: b/toe.L/toePivot.L/footControl.L
    m_Weight: 0
  - m_Path: b/toe.L/toePivot.L/footControl.L/footControl.L_end
    m_Weight: 0
  - m_Path: b/toe.R
    m_Weight: 0
  - m_Path: b/toe.R/toePivot.R
    m_Weight: 0
  - m_Path: b/toe.R/toePivot.R/footControl.R
    m_Weight: 0
  - m_Path: b/toe.R/toePivot.R/footControl.R/footControl.R_end
    m_Weight: 0
  - m_Path: faces
    m_Weight: 1
  - m_Path: pCube3
    m_Weight: 1
  - m_Path: pCube5
    m_Weight: 1
  - m_Path: pCube5.001
    m_Weight: 1
  - m_Path: pCube8
    m_Weight: 1
  - m_Path: SM_Axe
    m_Weight: 1
  - m_Path: SM_FishingRod
    m_Weight: 1
  - m_Path: SM_PickAxe
    m_Weight: 1
