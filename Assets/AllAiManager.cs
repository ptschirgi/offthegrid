using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum HardnessMode
{

    Easy,  //0-3
    Medium,  //4-7
    Hard,//8+
}

public class AllAiManager : MonoBehaviour
{
    public List<Monster> monsters = new List<Monster>();

    public List<EnemySpawnSpot> eSpawnSpots = new List<EnemySpawnSpot>();
    public Transform plyrPos;
    public Transform monsterParent;

    public bool startOff;
    public int monsterCount;

    public float speedMulto = 1f;

    public Monster monsterPrefab;
    public Monster monsterPrefab2;
    public Monster monsterPrefab3;

    void Start()
    {
        if (startOff)
        {

            for (int i = 0; i < monsters.Count; i++)
            {
                monsters[i].gameObject.SetActive(false);
            }
        }
    }

    public void DayTime(HardnessMode hardness)
    {
        switch (hardness)
        {
            case HardnessMode.Easy:
                {
                    break;
                }
            case HardnessMode.Medium:
                {
                    for (int s = 0; s < eSpawnSpots.Count; s++)
                    {

                        eSpawnSpots[s].covered = false;
                        // if()
                        // {

                        // }
                        eSpawnSpots[s].covery.SetActive(false);
                    }
                    break;
                }
            case HardnessMode.Hard:
                {
                    for (int s = 0; s < eSpawnSpots.Count; s++)
                    {

                        eSpawnSpots[s].covered = false;
                        eSpawnSpots[s].covery.SetActive(false);
                    }
                    break;
                }
        }


        for (int i = 0; i < monsters.Count; i++)
        {
            monsters[i].gameObject.SetActive(false);
        }
    }

    public void NightTime(HardnessMode hardness)
    {
        Monster newMo = Instantiate(monsterPrefab, Vector3.zero, Quaternion.identity);
        newMo.transform.parent = monsterParent;
        newMo.plyr = plyrPos;
        monsters.Add(newMo);

        switch (hardness)
        {
            case HardnessMode.Easy:
                {
                    break;
                }
            case HardnessMode.Medium:
                {
                    Monster newMo2 = Instantiate(monsterPrefab2, Vector3.zero, Quaternion.identity);
                    newMo2.transform.parent = monsterParent;
                    newMo2.plyr = plyrPos;

                    monsters.Add(newMo2);
                    break;
                }
            case HardnessMode.Hard:
                {
                    Monster newMo2 = Instantiate(monsterPrefab2, Vector3.zero, Quaternion.identity);
                    newMo2.transform.parent = monsterParent;
                    newMo2.plyr = plyrPos;

                    monsters.Add(newMo2);

                    Monster newMo3 = Instantiate(monsterPrefab3, Vector3.zero, Quaternion.identity);
                    newMo3.transform.parent = monsterParent;
                    newMo3.plyr = plyrPos;

                    monsters.Add(newMo3);
                    break;
                }
        }


        for (int i = 0; i < eSpawnSpots.Count; i++)
        {
            eSpawnSpots[i].used = false;
            eSpawnSpots[i].tempDistToPlayer = (eSpawnSpots[i].transform.position - plyrPos.position).magnitude;
        }

        eSpawnSpots.Sort((a, b) => a.tempDistToPlayer.CompareTo(b.tempDistToPlayer));



        for (int i = 0; i < monsters.Count; i++)
        {
            bool iOnce = false;

            for (int s = 0; s < eSpawnSpots.Count; s++)
            {
                if (!iOnce)
                {

                    if (!eSpawnSpots[s].covered)
                    {
                        if (!eSpawnSpots[s].used)
                        {
                            iOnce = true;
                            eSpawnSpots[s].used = true;
                            monsters[i].spawnObj = eSpawnSpots[s].transform;
                            monsters[i].transform.position = eSpawnSpots[s].transform.position - new Vector3(0, 5f, 0);
                            monsters[i].gameObject.SetActive(true);
                            monsters[i].SpawnoMode();
                        }
                    }
                }
            }

            if (!iOnce)
            {
                monsters[i].gameObject.SetActive(false);
            }
        }
    }

    public void PlayerDeadPlsLeave()
    {

    }
}
