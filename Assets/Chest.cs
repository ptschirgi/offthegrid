using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    public BoxCollider bx;
    public Animator anim;
    public PlayerChar p;

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {

            p.ChestHeal();
            bx.enabled = false;
            anim.SetTrigger("openChest");
        }
    }


}
